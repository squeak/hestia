
module.exports = {
  arrows: require("./arrows"),
  grapes: require("./grapes"),
  sandals: require("./sandals"),
};
