# All available presets
🛈 This page uses squyntax notation, information/documentation about it is available [here](/squyntax/). 🛈

Hestia simply returns an object listing presets that looks like this:
```javascript
hestia = {
  arrows: <{ [presetName<string>]: <arrow> }>,
  grapes: <grape[]>,
  sandals: <{ [presetName<string>]: <sandal> }>,
}
```

## Arrows

Arrows are presets containing configurations that can be converted by zeus to make [tablify](/tablify/) configurations.
For details on all the available presets, check the <arrows> directory in [hestia's repository](https://framagit.org/squeak/hestia/).


## Grapes

Grapes are presets containing types configurations, that can be converted by zeus to be added to populate the `entryTypes` option of [tablify](/tablify/) configurations.
For details on all the available presets, check the <grapes> directory in [hestia's repository](https://framagit.org/squeak/hestia/).


## Sandals

Sandals are presets containing configurations, that can be converted by zeus to [inputify](/inputify/) configurations.
For details on all the available presets, check the <sandals> directory in [hestia's repository](https://framagit.org/squeak/hestia/).
