
Hestia is the home for preset elements to feed Zeus's parts: Apollo, Dionisos and Hermes.

In other words, it contains preset Arrows, Grapes and Sandals.

Yet in other words, it contains part of configurations that put together by [Zeus](/zeus/), can be used to create databases displays through [tablify](/tablify/).
