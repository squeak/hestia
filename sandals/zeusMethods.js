var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/string");
var descartes = require("descartes");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULT MODEL FOR ARGUMENTS INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var defaultArgsModel = {
  inputifyType: "any",
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SETUP ARGUMENTS INPUTIFIED
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function setupArgumentsInput (chosenMethodName, argumentsInputified, listOfMethodsChoices) {

  // FIGURE OUT CHOSEN METHOD
  var methodChoice = _.findWhere(listOfMethodsChoices(), { value: chosenMethodName, });

  if (!methodChoice || !argumentsInputified) return;

  // NO ARGUMENTS REQUIRED
  if (_.isNull(methodChoice.arguments)) var argsOpts = {
    structure: [],
    model: undefined,
    hide: true,
  }
  // SPECIFIED STRUCTURED LIST OF ARGUMENTS
  else if (_.isArray(methodChoice.arguments) && methodChoice.arguments.length) var argsOpts = {
    structure: methodChoice.arguments,
    model: methodChoice.argumentsModel || defaultArgsModel,
  }
  // NO STRUCTURED LIST, MAYBE A MODEL OR NO CONSTRAINT
  else var argsOpts = {
    structure: undefined,
    model: methodChoice.argumentsModel || defaultArgsModel,
  };

  // SET VALUES TO ARGUMENTS SIBLING INPUTIFIED
  $$.setValue(argumentsInputified, "options.object.structure", argsOpts.structure);
  $$.setValue(argumentsInputified, "options.object.model", argsOpts.model);
  argumentsInputified.remake();

  // HIDE OR SHOW INPUT
  if (argsOpts.hide) argumentsInputified.hide()
  else argumentsInputified.show();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: return an input that let you setup a zeus method with arguments
    ARGUMENTS: (
      !zeusMethodKey <deepKey> « deepKey of this method in descartes methods list »,
      ?valueType <string> «
        the type to use for the value input
        if not defined, value input will be disable, only method side will be available
      »,
    )
    RETURN: <sandal>
  */
  zeusMethod: function (zeusMethodKey, valueType) {
    var listOfMethodsChoices = _.partial(descartes.zeusList, zeusMethodKey);
    var mandatoryArguments;
    return {
      // o            >            +            #            °            ·            ^            :            |

      //
      //                              INPUT TYPE

      inputifyType: "entry",

      object: {

        _recursiveOption: true,
        // pass the help message on top of the dialog
        help: function () {
          var inputified = this;
          return $$.string.htmlify(inputified.options.help);
        },

      },

      //
      //                              ENTRY OPTIONS: VALUE PROCESSING AND VERIFICATION

      entry: {
        customizedOptions: {

          inputifyType: "object",
          created: function () {
            var inputified = this;
            var typeInputified = inputified.getSubInputified("type");
            if (typeInputified && !valueType) typeInputified.hide();
          },
          object: {

            structure: [
              {
                // condition: valueType, // not using this, but the code in parent created function instead, because otherwise it ignores defaultValue... and value, method and arguments inputs don't display properly
                key: "type",
                labelLayout: "hidden",
                inputifyType: "radio",
                choices: ["value", "method"],
                defaultValue: valueType ? "value" : "method",
                modified: 'zeus+inputify.modified.doOnSiblings("recalculateDisplayCondition", ["value", "method", "arguments"])',
              },
              {
                key: "value",
                labelLayout: "intertitle",
                inputifyType: valueType || "normal",
                condition: !valueType ? false : 'zeus+inputify.condition.siblingValue("type", { "$eq": "value" }, true)',
                help: "Value to set.",
              },
              {
                key: "method",
                labelLayout: "intertitle",
                inputifyType: "select",
                help: "Choose here the method you want to use.",
                choices: function (callback) { callback(listOfMethodsChoices()); },
                condition: 'zeus+inputify.condition.siblingValue("type", { "$eq": "method" }, true)',
                modified: function (value) {
                  var inputified = this;
                  setupArgumentsInput(
                    value,
                    inputified.getSibling("arguments"),
                    listOfMethodsChoices
                  );
                },
              },
              {
                key: "arguments",
                labelLayout: "intertitle",
                // help: "Arguments to pass to the chosen method when it's executed.",
                condition: 'zeus+inputify.condition.siblingValue("type", { "$eq": "method" }, true)',
                inputifyType: "array",
                object: {
                  model: defaultArgsModel,
                  keepUndefinedEntries: true,
                },
              },
            ],

          },

          unvalidatedAlertError: false,
          valueOutVerify: function (value) {
            if (
              value &&
              value.type == "method" &&
              !_.isUndefined(mandatoryArguments) &&
              ($$.getValue(value, "arguments.length") || 0) != mandatoryArguments // or 0 because maybe just no arguments is defined and therfore arguments.length is undefined
            ) throw "you must specify "+ mandatoryArguments +" arguments"
            else return value;
          },
          valueOutProcess: function (value) {
            if (!value) return
            // return value
            if (value.type == "value") {
              mandatoryArguments = undefined;
              return value.value
            }
            // return method
            else {
              var result = "";
              if (value && _.isString(value.method)) result += value.method;
              if (result && value.arguments) result += JSON.stringify(value.arguments).replace(/^\[/, "(").replace(/\]$/, ")");
              if (result) return result;
            };
          },
          valueInProcess: function (string) {
            if (_.isString(string) && string.match(/^zeus\+/)) {
              var method = descartes.getMethodNameAndArguments(string);
              return {
                type: "method",
                method: method.prefix + method.deepKey,
                arguments: method.arguments,
              };
            }
            else if (valueType) {
              mandatoryArguments = undefined;
              return {
                type: "value",
                value: string,
              };
            }
            else return {
              type: "method",
            };
          },

        },
      },

      //                              ¬
      //

      // o            >            +            #            °            ·            ^            :            |
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
