var _ = require("underscore");
var $$ = require("squeak");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              NAME

  name: {
    key: "name",
    inputifyType: "normal",
  },

  //
  //                              NAME WITH TITLE PLACEHOLDER

  nameTitlePlaceholder: {
    key: "name",
    inputifyType: "normal",
    labelLayout: "hidden",
    input: {
      placeholder: "title",
    },
  },

  //
  //                              COLOR

  color: {
    key: "color",
    inputifyType: "color",
  },

  //
  //                              ICON

  icon: {
    key: "icon",
    inputifyType: "icon",
  },

  //
  //                              THUMBNAIL

  thumbnail: {
    key: "thumbnail",
    inputifyType: "file",
    file: {
      extensions: ["jpg", "jpeg", "png", "gif", "svg"],
      process: [ 'zeus+inputify.fileProcess.squareCrop()', 'zeus+inputify.fileProcess.resizeImage(400, 400, "png")' ],
    },
  },


  //
  //                              ORIENTATION

  orientation: {
    key: "orientation",
    help: "Orientation of the input.",
    inputifyType: "radio",
    choices: ["horizontal", "vertical"],
    prepareValue: "horizontal",
  },

  //
  //                              DETAILS

  meta: {
    key: "meta",
    inputifyType: "entry",
    labelLayout: "stacked",
    defaultValue: function () {
      return {
        date: $$.date.make().format("YYYY-MM-DD_HH:mm"),
      };
    },
    object: {
      structure: [

        {
          key: "date",
          inputifyType: "date",
          labelLayout: "stacked",
          date: {
            precisionChoiceDisable: false,
          },
        },

        {
          key: "color",
          inputifyType: "color",
          labelLayout: "stacked",
        },

        {
          key: "icon",
          inputifyType: "icon",
          labelLayout: "stacked",
        },

        {
          key: "thumbnail",
          inputifyType: "file",
          labelLayout: "stacked",
          file: {
            extensions: ["jpg", "jpeg", "png", "gif", "svg"],
            process: [ 'zeus+inputify.fileProcess.squareCrop()', 'zeus+inputify.fileProcess.resizeImage(400, 400, "png")' ],
          },
        },

        {
          key: "tags",
          inputifyType: "multi",
          labelLayout: "stacked",
          choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
        },

      ],
    },
  },

  //
  //                              GROUP

  group: {
    key: "group",
    inputifyType: "simple",
    labelLayout: "intertitle",
    isAdvanced: true,
    choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
    suggest: {
      labelToValue: function (val) {
        if (val == "_system") throw "you cannot use the group '_system', it's reserved for system types";
        return val;
      },
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
