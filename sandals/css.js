
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CSS RULES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  cssRules: {
    key: "cssRules",
    help: [
      "Global rules you want to apply to this table/folder display.",
      "Each key of this object, is the css selector to use.",
      "This selector will be prepended by the class of this tablify container, so the styles can only be applied to it's children elements, or to itself using & like lesscss does.",
    ],
    isAdvanced: true,
    monospace: true,
    inputifyType: "entry",
    object: {
      shape: "array",
      sortDisplay: "selector",
      model: {
        inputifyType: "object",
        object: {
          structure: [
            {
              key: "selector",
              inputifyType: "normal",
              help: "The selector of the elements you want to apply the rules to. (e.g. \".my-class\", or \"&.tablify-gallery\" if you wanted to customize display of the tablify element when in gallery display).",
              monospace: true,
            },
            {
              key: "rule",
              inputifyType: "textarea",
              help: "The properties(s) to set (e.g. \"background-color: red;\"...)",
              monospace: true,
            },
            {
              key: "mediaQuery",
              inputifyType: "normal",
              help: "A custom media query to wrap your rule in and use it only in some devices (e.g. \"@media (min-width: 750px)\").",
              monospace: true,
            },
          ],
        },
      },
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
