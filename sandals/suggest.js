var _ = require("underscore");
var $$ = require("squeak");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              SUGGEST VALUES IN DATABASE

  suggestInDatabase_simple: {
    inputifyType: "simple",
    choices: "zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey",
  },

  suggestInDatabase_multi: {
    inputifyType: "multi",
    choices: "zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey",
  },

  //
  //                              SUGGEST AVAILABLE TYPES IN DATO HOME DB

  // ⚠️ works only from inside the main home database
  availableTypes: {
    choices: function (callback) {
      var inputified = this;
      var Collection = inputified.storageRecursive("postified.storage.tablifyApp.Collection");
      var systemTypesToSuggest = [ "link", "note", "todo", "contact", "location", "bookmark", "separator", "subject", "monitoring" ];
      var pouchTypes = _.chain(Collection.toJSON()).where({ type: "type", }).pluck("name").value();
      var choices = _.chain([systemTypesToSuggest, pouchTypes]).flatten().compact().uniq().value();
      callback(choices);
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
