var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/color");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SUBJECTS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var todoStatus2Color = {
  todo: "#0F0",
  todont: "#F00",
  totry: "#1effb9",
  toflex: "#f2d4ff", // ff1eef
  toprocess: "#25dcf2",
};
function subjectsList (key) {
  var colorObj = {
    primary: todoStatus2Color[key],
    primaryContrast: $$.color.contrast(todoStatus2Color[key]),
    base: "black",
    baseContrast: "white",
  };
  return _.compact([

    //
    //                              TODO

    {
      key: "todo",
      inputifyType: "radio",
      labelLayout: "hidden",
      help: "toprocess|todo|totry|toflex|todont",
      radioTickbox: { iconChecked: false, },
      choices: [
        { value: "toprocess",  label: '<span class="icon-question-circle"></span>',    _isChoice: true, },
        { value: "todo",       label: '<span class="icon-thumb-tack"></span>',         _isChoice: true, }, // thumbs-up
        { value: "totry",      label: '<span class="icon-question-circle"></span>',    _isChoice: true, },
        { value: "toflex",     label: '<span class="icon-icecream"></span>',           _isChoice: true, },
        { value: "todont",     label: '<span class="icon-blocked"></span>',            _isChoice: true, }, // exclamation-triangle notice thumbs-down
      ],
      color: colorObj,
    },

    //
    //                              SHOULDN'T HAVE DONE IT, BUT DID IT

    key !== "todont" ? undefined : {
      key: "didDespiteSayingDont",
      inputifyType: "boolean",
      labelLayout: "hidden",
      help: "you did it despite planning not to",
      // color: { primary: "#F00", hover: "#FF0", baseContrast: "#000", },
      boolean: { iconOn: "spaceinvaders", iconOff: "spaceinvaders", }, // binocular archive cabinet badge2 neutral bomb exclamation-triangle database gamepad2
    },

    //
    //                              ID (CONTEXT/NAME)

    {
      key: "subject",
      labelLayout: "hidden",
      inputifyType: "select",
      cssContainer: { flexGrow: 1, },
      cssInput: {
        fontWeight: "bold",
        textAlign: "center",
      },
      choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries({"type":"subject"},"_id","{{ context}}/{{ name }}")',
      color: colorObj,
    },

    //
    //                              NAME

    {
      key: "frequency",
      labelLayout: "hidden",
      inputifyType: "select",
      help: ["multidaily|daily|otherdaily|weekly", "monthly|quarterly|yearly|occasional"],
      choices: [
        { value: "multidaily",  label: '<span title="multidaily" class="icon-dashboard"></span>',      _isChoice: true, },
        { value: "daily",       label: '<span title="daily"      class="icon-time"></span>',           _isChoice: true, },
        { value: "otherdaily",  label: '<span title="otherdaily" class="icon-alarm"></span>',          _isChoice: true, },
        { value: "weekly",      label: '<span title="weekly"     class="icon-alarm2"></span>',         _isChoice: true, },
        { value: "monthly",     label: '<span title="monthly"    class="icon-calendar3"></span>',      _isChoice: true, },
        { value: "quarterly",   label: '<span title="quarterly"  class="icon-hourglass"></span>',      _isChoice: true, },
        { value: "yearly",      label: '<span title="yearly"     class="icon-birthday-cake"></span>',  _isChoice: true, },
        { value: "occasional",  label: '<span title="occasional" class="icon-icecream"></span>',       _isChoice: true, },
      ],
      color: colorObj,
    },

    //
    //                              PRIORITY

    {
      key: "priority",
      inputifyType: "select",
      labelLayout: "hidden",
      choices: [
        { value: "low",    label: '<div style="background-color: #cacaca"></div>',  _isChoice: true, }, // number11 battery
        { value: "normal", label: '<div style="background-color: #25dcf2"></div>',  _isChoice: true, }, // number12 battery2
        { value: "high",   label: '<div style="background-color: #F00"></div>',     _isChoice: true, }, // number13 battery3
      ],
      help: "low|normal|high",
      cssContainer: { "break-after": "always", }, // break flex line after this input
    },

    //
    //                              STATUS

    _.indexOf(["todo", "totry", "toflex"], key) === -1 ? undefined : {
      key: "status",
      inputifyType: "radio",
      labelLayout: "hidden",
      radioTickbox: { iconChecked: false, },
      choices: [
        { value: "done",       label: '<span class="icon-check"></span>',       _isChoice: true, },
        { value: "partial",    label: '<span class="icon-pie-chart"></span>',   _isChoice: true, },
        { value: "postponed",  label: '<span class="icon-hourglass2"></span>',  _isChoice: true, }, // alarmclock, history, hourglass
        { value: "abandonned", label: '<span class="icon-cross"></span>',       _isChoice: true, }, // trash
      ],
      // choices: ["✓", "⏰", "🗑"],
      help: "done|partial|postponed|abandonned",
      condition: 'zeus+inputify.condition.siblingValue("todo", { "$in": ["todo", "totry", "toflex"] }, true)',
      siblingsSubscriptions: [
        { key: "todo", event: "modified", action: "remake", },
      ],
    },

    //
    //                              DURATION PLAN

    {
      key: "durationPlan",
      labelLayout: "hidden",
      inputifyType: "normal",
      input: {
        size: 1, // prevent input to have a default width of 100px
        placeholder: "duration planned",
      },
      cssContainer: { flexGrow: 1, },
      cssInput: { textAlign: "center", },
      defaultValue: 'zeus+inputify.value.subvalueInParentInput("durationPlan")', // set a defaultValue otherwise when input is hidden/shown it loses it's value
      condition: 'zeus+inputify.condition.siblingValue("todo", { "$in": ["todo", "totry", "toflex"] }, true)',
      siblingsSubscriptions: [
        { key: "todo", event: "modified", action: "remake", },
      ],
    },

    //
    //                              DURATION DONE

    key === "toprocess" ? undefined : {
      key: "durationDone",
      labelLayout: "hidden",
      inputifyType: "normal",
      input: {
        size: 1, // prevent input to have a default width of 100px
        placeholder: "duration done",
      },
      cssContainer: { flexGrow: 1, },
      cssInput: { textAlign: "center", },
      defaultValue: 'zeus+inputify.value.subvalueInParentInput("durationDone")', // set a defaultValue otherwise when input is hidden/shown it loses it's value
      condition: _.indexOf(["todo", "totry", "toflex"], key) !== -1 ? 'zeus+inputify.condition.siblingValue("status", { "$in": ["done", "partial"] }, true)' : 'zeus+inputify.condition.siblingValue("didDespiteSayingDont", true)',
      siblingsSubscriptions: [
        { key: "status", event: "modified", action: "remake", },
        { key: "didDespiteSayingDont", event: "modified", action: "remake", },
      ],
    },

    //
    //                              FEEDBACK

    // REMOVED TO USE A SIMPLER MORE GLOBAL INDICATOR FOR WHOLE PERIOD
    // key === "toprocess" ? undefined : {
    //   key: "feedback",
    //   labelLayout: "hidden",
    //   inputifyType: "radio",
    //   help: "stress|speed|peace|energy",
    //   cssContainer: { flexGrow: 1, },
    //   // condition: 'zeus+inputify.condition.siblingValue("status", { "$exists": true }, true)',
    //   // // condition: 'zeus+inputify.condition.siblingsValues([{ "siblingKey": "todo", "siblingValue": true }, { "siblingKey": "done", "siblingValue": true }], "||")',
    //   // siblingsSubscriptions: [
    //   //   // { key: "todo", event: "modified", action: "remake", },
    //   //   { key: "status", event: "modified", action: "remake", },
    //   // ],
    //   radioTickbox: { iconChecked: false, },
    //   choices: [
    //     { value: "stress",  label: '<span class="icon-pacman2"></span>',  _isChoice: true, }, // street-view radioactive
    //     { value: "speed",   label: '<span class="icon-wind"></span>',     _isChoice: true, }, // paper-plane rocket2
    //     { value: "peace",   label: '<span class="icon-anchor"></span>',   _isChoice: true, }, // anchor
    //     { value: "energy",  label: '<span class="icon-power"></span>',    _isChoice: true, }, // power
    //   ],
    // },

    //                              ¬
    //

  ]);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SUBJECTS LIST INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function subjectsListInput (key, defaultValue) {
  return {

    // INPUT CONFIG
    key: key,
    inputifyType: "array",
    labelLayout: "intertitle",
    object: {
      sortDisplay: "name",
      model: {
        inputifyType: "object",
        labelLayout: "hidden",
        inputClass: "hestia-monitoring-subjects_list_input",
        containerClass: "hestia-monitoring-subjects_list_input_container",
        object: { structure: subjectsList(key), },
      },
    },

    // DEFAULT VALUE
    defaultValue: defaultValue || [],
    // defaultValue: "zeus+inputify.value.fillFromValuesInOtherEntry({\"type\":\"subject\"},\"{ \\\"name\\\": \\\"{{ context }}/{{ name }}\\\", \\\"todo\\\": false, \\\"done\\\": false }\", true)",
    defaultOnlyForNew: true,

    // COLORING
    color: {
      primary: todoStatus2Color[key],
      primaryContrast: $$.color.contrast(todoStatus2Color[key]),
    },

    // MOVE SUBJECTS TO SIBLINGS ON CLICK
    // labelAdditionalButtons: [
    //   {
    //     icomoon: "refresh",
    //     title: "refresh moving to todo/todont",
    //     click: refreshRepartitionToSiblings,
    //   },
    // ],

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MONITORING ICONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var monitoringFeedbackIcons = {
  stress: "pacman2",   // street-view radioactive
  speed: "wind",       // paper-plane rocket2
  peace: "anchor",     // anchor
  energy: "power",     // power
  mix: "command",      // expand street-view washingmachine
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "monitoring", // record mark supervision evaluation pursuing seeking following examination once-over assessment awareness gathering
  group: "_basics",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { icon: "lamp2", }, // streetlight
    title: 'zeus+display.title.valueOrResult("dateRange", "unnamed")',
    // color: 'zeus+display.color.result("#dddddd")',
    color: 'zeus+display.color.valueToColor("alienation", '+ JSON.stringify({ 0: "#0F0", 1: "#baff00", 2: "#fff900", 3: "#FFA217", 4: "#F00", 5: "#bc00ff", }) +', "#dddddd")',
    // icon: 'zeus+display.icon.result("lamp2")',
    icon: 'zeus+display.icon.valueToIcon("feedback", '+ JSON.stringify(monitoringFeedbackIcons) +', "lamp2")',
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      //
      //                              DATE & COMMENTS

      {
        key: "dateRange",
        inputifyType: "dateRange",
        labelLayout: "hidden",
        defaultValue: 'zeus+inputify.value.todayLate',
      },

      {
        key: "feedback",
        labelLayout: "hidden",
        inputifyType: "radio",
        help: _.keys(monitoringFeedbackIcons).join("|"),
        cssContainer: { flexGrow: 1, },
        radioTickbox: { iconChecked: false, },
        choices: _.map(monitoringFeedbackIcons, function (icon, key) {
          return { value: key,  label: '<span class="icon-'+ icon +'"></span>', _isChoice: true, };
        }),
      },

      {
        key: "comments",
        inputifyType: "textarea",
        labelLayout: "hidden",
        input: { placeholder: "comments", },
      },

      {
        key: "alienation",
        inputifyType: "slider",
        // labelLayout: "hidden",
        slider: {
          min: 0,
          max: 5,
          step: 1,
        },
      },

      {
        key: "archive",
        inputifyType: "boolean",
        defaultValue: false,
      },

      //
      //                              SUBJECTS

      subjectsListInput("toprocess", 'zeus+inputify.value.fillMonitoringFromSubjects(["schedule"])'),
      // subjectsListInput("todo", 'zeus+inputify.value.fillMonitoringFromSubjects(["schedule", "ignore"], true)'),
      subjectsListInput("todo"),
      subjectsListInput("totry"),
      subjectsListInput("toflex", 'zeus+inputify.value.fillMonitoringFromSubjects(["flex"])'),
      subjectsListInput("todont", 'zeus+inputify.value.fillMonitoringFromSubjects(["ignore"])'),

      //                              ¬
      //

    ],
    beforeSave: 'zeus+edify.beforeSave.monitoringDispatchSubjects()',
    buttons: [
      {
        icomoon: "stack-overflow", // copy
        title: "copy todo list to clipboard",
        click: 'zeus+edify.menubarButtonClick.copyTodosToClipboard()',
      },
      {
        icomoon: "refresh",
        title: "refresh dispatching of subjects",
        click: 'zeus+edify.menubarButtonClick.monitoringDispatchSubjects()',
      },
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  view: {
    fields: [
      {
        deepKey: "comments",
      },
      {
        display: "zeus+viewify.fieldDisplay.subjectsMonitoringStats",
      },
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
