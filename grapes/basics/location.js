
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "location",
  group: "_basics",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { icon: "location", },
    title: 'zeus+display.title.valueOrResult("name", "unnamed")',
    subtitle: 'zeus+display.title.joined("meta.tags", "")',
    color: 'zeus+display.color.valueOrResult("meta.color")',
    icon: 'zeus+display.icon.valueOrResult("meta.icon", "circle2")', // list2
    image: 'zeus+display.image.imageAttachedToEntry("meta.thumbnail")',
    additional: {
      "geoKey": 'zeus+display.geoKey.result("coordinates")',
      "weight": 'zeus+display.additional.valueOrResult("meta.weight")',
      "strokeDashArray": 'zeus+display.additional.valueOrResult("meta.lineType")',
      "fillColor": 'zeus+display.additional.valueOrResult("meta.fillColor")',
      "fillOpacity": 'zeus+display.additional.valueOrResult("meta.fillOpacity")',
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      //
      //                              NAME

      "nameTitlePlaceholder",

      //
      //                              DESCRIPTION

      {
        key: "description",
        inputifyType: "textarea",
        labelLayout: "hidden",
        input: { placeholder: "description", },
        cssInput: { height: 200, },
      },

      //
      //                              COORDINATES

      {
        key: "coordinates",
        inputifyType: "geo",
        labelLayout: "stacked",
      },

      //
      //                              META

      {
        key: "meta",
        label: "details and shape",
        inputifyType: "entry",
        labelLayout: "stacked",
        object: {
          structure: [

            //
            //                              GENERAL

            { key: "FOR ALL TYPES OF LOCATIONS", inputifyType: "intertitle", },

            "color",
            "icon",
            "thumbnail",

            {
              inputifyType: "slider",
              key: "weight",
              slider: {
                "min": 0,
                "max": 10,
                "step": 1,
              },
              help: "Use this to specify the size of an icon, or thickness of lines and contours of surfaces.",
            },

            {
              key: "tags",
              inputifyType: "multi",
              choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
            },

            //
            //                              FOR LINES AND SURFACES ONLY

            { key: "FOR LINES AND SURFACES", inputifyType: "intertitle", },

            {
              inputifyType: "normal",
              key: "lineType",
              help: "With this you can create different types of dashed lines. You should pass here a list of comma separated numbers specifying the lenght of lines and length of voids (e.g. \"4, 10\" or a more complex pattern: \"10, 5, 20, 25\")",
            },

            //
            //                              FOR SURFACES ONLY

            { key: "FOR SURFACES", inputifyType: "intertitle", },

            {
              inputifyType: "color",
              key: "fillColor",
              help: "Color applied to fill shape. If undefined, the main color will be used.",
            },
            {
              key: "fillOpacity",
              inputifyType: "slider",
              slider: {
                "min": 0,
                "max": 1,
                "step": 0.01,
              },
              help: "How opaque should be the fill color.",
            },

            //                              ¬
            //

          ],
        },
      },

      //
      //                              ATTACHMENTS

      {
        key: "attachments",
        inputifyType: "files",
        labelLayout: "stacked",
        file: {
          process: [ 'zeus+inputify.fileProcess.resizePrompt()' ],
          title: true,
        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  view: {
    fields: [
      {
        deepKey: "name",
        css: {
          "font-size": "1.3em",
          "font-weight": "bold",
          "color": "#149d08",
        },
      },
      {
        deepKey: "description",
        display: "zeus+viewify.fieldDisplay.markdown",
      },
      {
        deepKey: "attachments",
        display: "zeus+viewify.fieldDisplay.anyAttachments()",
        // display: "zeus+viewify.fieldDisplay.attachedImagesSwipable()"
        labelLayout: "inline",
      },
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
