var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "bookmark",
  group: "_basics",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { icon: "bookmark-o", },
    title: "zeus+display.title.interpolatedString(\"{{ name }} {{ \\\"<span class=\\\"\\\"descartes-front_primary\\\"\\\">\\\" rating \\\"</span>\\\" }}\",{\"name\":\"unamed\"},\"\")",
    subtitle: 'zeus+display.title.valueOrResult("url")',
    color: 'zeus+display.color.valueOrResult("meta.color")',
    icon: 'zeus+display.icon.valueOrResult("meta.icon", "bookmark-o")', // bookmarks
    // icon: function (entry, typeConfig) {
    //   if (entry.meta && entry.meta.icon) return entry.meta.icon
    //   // if ($$.match(entry.url, /(github\.com)|(github\.io)/)) return "github"
    //   // else if ($$.match(entry.url, /^https\:\/\/gitlab\.com/)) return "gitlab"
    //   // else if ($$.match(entry.url, /(youtube\.com)|(youtu\.be)/)) return "youtube3"
    //   // else if ($$.match(entry.url, /yunohost\.org/)) return "yahoo"
    //   else if (entry.description) return "bookmarks"
    //   else return "bookmark-o";
    // },
    image: 'zeus+display.image.imageAttachedToEntry("meta.thumbnail")',
    additional: {
      buttons: [{
        title: "Open the view window of this entry.",
        icomoon: "eye",
        condition: 'zeus+display.buttonCondition.valuesExist(["description"])',
        click: 'zeus+display.buttonClick.editEntry()',
      }],
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.copyValueToClipboard("%%url%%")',
    storeInHash: ["edit"], // this is set so that tablify do not store in hash any viewed entry (since view display is not used)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      "nameTitlePlaceholder",

      {
        inputifyType: "normal",
        key: "url",
        labelLayout: "hidden",
        input: { placeholder: "url", },
      },

      {
        inputifyType: "textarea",
        key: "description",
        labelLayout: "hidden",
        input: { placeholder: "description", },
        cssInput: { height: 200, },
      },

      {
        inputifyType: "radio",
        key: "rating",
        labelLayout: "stacked",
        choices: ["✶", "✶✶", "✶✶✶", "✶✶✶✶", "✶✶✶✶✶"],
      },

      {
        key: "attachments",
        inputifyType: "files",
        labelLayout: "stacked",
        file: {
          process: [ 'zeus+inputify.fileProcess.resizePrompt()' ],
          title: true,
        },
      },

      "meta",

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
