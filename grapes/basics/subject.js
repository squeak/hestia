var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "subject", // field matter subject matiere
  group: "_basics",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { icon: "cone", },
    title: 'zeus+display.title.interpolatedString("{{ context }}/{{ name }}")',
		subtitle: 'zeus+display.title.interpolatedString("{{ frequency }} - {{ duration }}")',
    color: 'zeus+display.color.valueToColor("priority", '+ JSON.stringify({
      high:       "#F00",     // red
      normal:     "#25dcf2",  // light blue
      low:        "#cacaca",  // grey
    }) +')',
    // color: 'zeus+display.color.randomColorFromAnyValueWithMatching("context", "/^[^\:\/]*/", null, "#acacac")',
    // not using custom colors, because too much overlapping, same colors used for different strings
    // color: 'zeus+display.color.randomColorFromAnyValueWithMatching("context", "/^[^\:\/]*/", ['+
    //   '"#00eb84","#0bf0ed","#fffd00","#fc00c7","#ff7f00","#0084eb","#ff0000","#ffffff","#aa00ff",'+
    //   '"#7d5656","#ffa800","#20a30c","#14b177","#bfa3a3","#8dcdff","#626262","#acacac"'+
    // '])',
    // color: 'zeus+display.color.valueToColor("time", {'+
    //   '"schedule": "#ff8600",'+
    //   '"flex":"#00ff05"'+
    // '}, "#0be2e7")',
    // icon: 'zeus+display.icon.valueToIcon("organizing", '+ JSON.stringify({
    //   ignore:   "blocked",
    //   flex:     "icecream",
    //   schedule: "auction",
    // }) +', "cone")',
    icon: 'zeus+display.icon.valueToIcon("frequency", '+ JSON.stringify({
      multidaily:  "dashboard",
      daily:       "time",
      otherdaily:  "alarm",
      weekly:      "alarm2",
      monthly:     "calendar3",
      quarterly:   "hourglass",
      yearly:      "birthday-cake",
      occasional:  "icecream",
    }) +', "cone")',
    additional: {
      buttons: [
        {
          title: "Should be scheduled!",
          icomoon: "navigation", // universal-access
          condition: 'zeus+display.buttonCondition.valuesMatch({"time":"schedule"})',
          click: 'zeus+display.buttonClick.alertValues(["context", "name", "description", "need", "frequency", "duration", "durationComment"], true)',
          css: { color: "#ff8600", },
        },
        {
          title: "Check list location.",
          icomoon: "list-ul",
          condition: 'zeus+display.buttonCondition.valuesExist(["list"])',
          click: 'zeus+display.buttonClick.alertValues(["list"])',
        },
        {
          title: "Check duration details.",
          icomoon: "chronometer",
          condition: 'zeus+display.buttonCondition.valuesExist(["durationComment"])',
          click: 'zeus+display.buttonClick.alertValues(["duration", "durationComment"])',
        },
        {
          title: "Check need conditions.",
          icomoon: "attachment2",
          condition: 'zeus+display.buttonCondition.valuesExist(["need"])',
          click: 'zeus+display.buttonClick.alertValues(["need"])',
        },
      ],
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.edit()',
    // storeInHash: ["edit"], // this is set so that tablify do not store in hash any viewed entry (since view display is not used)
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      //
      //                              GENERAL

      { key: "GENERAL", inputifyType: "intertitle", },

      {
        key: "context",
        inputifyType: "simple",
        labelLayout: "hidden",
        choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
        help: "Subject context/group.",
      },

      {
        key: "name",
        inputifyType: "normal",
        labelLayout: "hidden",
        input: { placeholder: "subject name", },
      },

      {
        key: "description",
        inputifyType: "textarea",
        labelLayout: "hidden",
        input: { placeholder: "description", },
      },

      //
      //                              DETAILS

      { key: "DETAILS", inputifyType: "intertitle", },

      {
        key: "list",
        inputifyType: "multi",
        choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
        help: "The link to the todo list(s) for this subject."
      },

      {
        key: "need",
        inputifyType: "multi",
        choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
        help: "Necessary items, contexts for this subject to be actionnable."
      },

      {
        key: "priority",
        inputifyType: "radio",
        choices: ["low", "normal", "high"],
        help: "Subject's current priority.",
      },

      //
      //                              TIMING

      { key: "TIMING", inputifyType: "intertitle", },

      {
        key: "organizing",
        inputifyType: "radio",
        choices: ["ignore", "flex", "schedule"],
        help: [
          'This field helps dato to pre-organize your subjects in "monitoring" entries:',
          ' - "schedule" means this subject needs to be actively scheduled (= automatically put in "toprocess" list)',
          ' - "flex" means this subject is scheduling itself, or doesn\'t need any specific attention (= automatically put in "toflex" list)',
          ' - "ignore" means this subject is currently ignored, not practiced (= automatically put in "todont" list)',
        ],
      },

      {
        key: "frequency",
        inputifyType: "select",
        choices: ["multidaily", "daily", "otherdaily", "weekly", "monthly", "quarterly", "yearly", "occasional"],
        help: "The frequency at which you want to practice this subject.",
      },

      {
        key: "duration",
        inputifyType: "simple",
        choices: 'zeus+inputify.choices.valuesInThisDatabaseEntries_sameKey()',
        help: [
          "For proper subjects stats, you should input durations that look like some any of the following examples:",
          '"1day" "2days" "3h" "~5hours" "2days+" "10min" "10m" "10minutes" "15min-1h"',
        ],
      },

      {
        key: "durationComment",
        inputifyType: "textarea",
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
