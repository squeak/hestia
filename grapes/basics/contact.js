var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "contact",
  group: "_basics",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { "icon": "user-circle" },
    title: 'zeus+display.title.valueOrResult("name","NO NAME")',
    subtitle: 'zeus+display.title.valueOrResult("comment")',
    color: 'zeus+display.color.randomColorFromAnyValue("name", null, "#FFFFFF88")',
    icon: 'zeus+display.icon.result("user-circle-o")',
    image: 'zeus+display.image.imageAttachedToEntry("image")',
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      //
      //                              MAIN

      {
        inputifyType: "normal",
        key: "name",
        input: { placeholder: "name" },
        labelLayout: "hidden"
      },
      {
        inputifyType: "textarea",
        key: "comment",
        input: { placeholder: "comment" },
        labelLayout: "hidden"
      },

      //
      //                              DETAILS

      { key: "DETAILS", inputifyType: "intertitle", },

      {
        inputifyType: "array",
        key: "phone",
        label: "Phone numbers:",
        labelLayout: "stacked",
        object: {
          model: {
            inputifyType: "phone",
            input: { placeholder: "+12 345 678 90" },
          },
        },
      },

      {
        inputifyType: "array",
        key: "email",
        label: "Email adresses:",
        labelLayout: "stacked",
        object: {
          model: {
            inputifyType: "email",
            input: { placeholder: "user@domain.tld" },
          },
        },
      },

      {
        inputifyType: "array",
        key: "address",
        label: "Addresses:",
        labelLayout: "stacked",
        object: {
          model: {
            inputifyType: "textarea",
            input: { placeholder: "Somewhere street 1st\nOuagadougou\nBurkina Faso" },
          },
        },
      },

      {
        inputifyType: "array",
        key: "website",
        label: "Websites:",
        labelLayout: "stacked",
        object: {
          model: {
            inputifyType: "url" ,
            input: { placeholder: "https://example.org" },
          },
        },
      },

      //
      //                              ADDITIONAL

      { key: "ADDITIONAL", inputifyType: "intertitle", },

      {
        inputifyType: "file",
        key: "image",
        label: "Photo:",
        labelLayout: "inline",
        file: {
          fileTypes: [ "image" ],
          process: [
            "zeus+inputify.fileProcess.squareCrop",
            "zeus+inputify.fileProcess.resizeImage(500,500,null,0.91)"
          ],
        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VIEW
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  view: {
    fields: [

      //
      //                              IMAGE

      {
        display: 'zeus+viewify.fieldDisplay.attachedImage({"width":"60%","margin-left":"20%"})',
        deepKey: "image"
      },

      //
      //                              MAIN

      {
        deepKey: "name",
        css: {
          "font-size": "1.5em",
          "font-weight": "bold"
        },
      },

      {
        deepKey: "comment"
      },

      //
      //                              DETAILS

      {
        deepKey: "phone",
        label: "Phone numbers:",
        labelLayout: "stacked",
        display: 'zeus+viewify.fieldDisplay.customMethod("tapToCopyArrayEntries")',
      },
      {
        deepKey: "address",
        label: "Addresses:",
        labelLayout: "stacked",
        display: 'zeus+viewify.fieldDisplay.customMethod("tapToCopyArrayEntries")',
      },
      {
        deepKey: "email",
        label: "Email addresses:",
        labelLayout: "stacked",
        display: 'zeus+viewify.fieldDisplay.customMethod("tapToCopyArrayEntries")',
      },
      {
        deepKey: "website",
        label: "Websites:",
        labelLayout: "stacked",
        display: 'zeus+viewify.fieldDisplay.customMethod("tapToCopyArrayEntries")',
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
