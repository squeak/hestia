var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_data_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {

    creationMenu: {
      name: "data",
      icon: "database",
      key: "d",
    },

    title: 'zeus+display.title.valueOrResult("name")',
    icon: 'zeus+display.icon.result("database")',
    additional: {
      secondaryColor: 'zeus+display.icon.result("#ff3165")',
    },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.edit',
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {

    structure: [

      // o            >            +            #            °            ·            ^            :            |
      //                                           NAME AND GROUP

      {
        key: "name",
        inputifyType: "normal", // TODO: should not allow to make two data entries with same name ??
        label: "DATA STORE NAME",
        labelLayout: "intertitle",
        mandatory: true,
        help: [
          "Name of this data store.",
          '<span class="error-text">Choose this wisely because it will not be modifiable in the future.</span>',
          "Should contain only letters, numbers, underscores (_), and cannot start with a number.",
        ],
        locked: function () { return !this.storageRecursive("isNewEntry"); },
        valueOutVerify: function (val) {
          if (!val || !_.isString(val)) throw "you must choose a name for this data store"
          else if (val.match(/\W/) || val.match(/\d/)) throw "the name can only contain letters, numbers, underscores (_), and should not start with a number";
          return val;
        },
      },

      {
        inputPreset: "group",
        help: "The group this data belongs to."
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           SHAPE OF THIS DATA STORE

      {
        key: "datashape",
        labelLayout: "intertitle",
        inputifyType: "entry",
        object: {
          structure: [

            //
            //                              SHAPE

            {
              label: "SHAPE",
              labelLayout: "intertitle",
              key: "shape",
              inputifyType: "radio",
              choices: ["array", "object"],
            },

            //
            //                              STRUCTURE

            {
              label: "STRUCTURE",
              labelLayout: "intertitle",
              key: "structure",
              inputifyType: "array",
              object: {
                model: {
                  inputifyType: "entry",
                  entryType: "_inputify_",
                },
                sortDisplay: "key",
              },
            },

            //
            //                              MODEL

            {
              label: "MODEL",
              labelLayout: "intertitle",
              key: "model",
              help: [
                "If you want all inputs in this object/array to be of the same type, you can specify it here.",
                "If structure is defined, this input configuration will be used for new custom keys.",
              ],
              inputifyType: "entry",
              entryType: "_inputify_withoutKey_",
            },

            //
            //                              SORT DISPLAY

            {
              label: "SORT DIALOG DISPLAY",
              labelLayout: "intertitle",
              key: "sortDisplay",
              siblingsSubscriptions: [ { key: "shape", event: "modified", action: "remake", }, ],
              condition: function () {
                var inputified = this;

                // shape defined
                var shapeValue = inputified.getSiblingValue("shape");
                if (shapeValue) {
                  if (shapeValue == "array") return true
                  else if (shapeValue == "object") return false;
                };
              },
              help: [
                "You can use this to customize what's displayed when the user asks to reorgannize entries in this array.",
                "When the reogrannize dialog is opened, it lists the entries in this array so you can grab them and change their order. That list just show one line of each entry.",
                "Set here the key where to find in your entry the content that should be shown in that line.",
              ],
              inputifyType: "normal",
            },

            //                              ¬
            //

          ],
        },
        modified: function () {
          var inputified = this;
          var dataInputified = inputified.getSibling("data");
          if (dataInputified) {
            dataInputified.options.object = inputified.get();
            dataInputified.remake();
          };
        },
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           THE ACTUAL DATA

      {
        key: "data",
        labelLayout: "intertitle",
        inputifyType: "entry",
        object: {
          // content of this will be defined by the preferences set in "datashape"
        },
        // the following "created" function may be useless, because it's probable that "datashape" input won't be ready in time
        // but in that case the "modified" function from "datashape" input will be run appropriately
        created: function () {
          var inputified = this;
          inputified.options.object = inputified.getSiblingValue("datashape");
          inputified.remake();
        },
      },

      //                                            ¬
      // o            >            +            #            °            ·            ^            :            |

    ],

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
