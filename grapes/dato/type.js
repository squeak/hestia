var _ = require("underscore");
var $$ = require("squeak");
var datoTypes = ["default", "folder", "database", "database-home", "link", "type"];

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "type",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: {
      name: "type",
      icon: "profile5",
      key: "t",
    },
    title: 'zeus+display.title.valueOrResult("name")',
    icon: 'zeus+display.icon.result("profile5")', // sliders
    // color: 'zeus+display.icon.result("#FA0")',
    additional: {
      secondaryColor: 'zeus+display.icon.result("#AD7B66")',
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.edit',
    // TODO: could reload appropriate database when a type it contains is changed, but it would take time to implement, and could even be an annoying thing in fact
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    doNotAutoAddPath: true,
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      //
      //                              NAME AND GROUP

      {
        key: "name",
        label: "TYPE NAME",
        labelLayout: "intertitle",
        inputifyType: "normal",
        mandatory: true,
        locked: function () { return !this.storageRecursive("isNewEntry"); },
        valueOutVerify: function (val) {
          if (!val || !_.isString(val)) throw "you must choose a name for this type"
          else if (val.match(/^_/) && val.match(/_$/)) throw "you cannot use a name starting and ending with an underscore, they are reserved for the system"
          else if (_.indexOf(datoTypes, val) != -1) throw "the name you chose is reserved for the system (the following names cannot be used: "+ datoTypes.join(", ") +")"
          return val;
        },
        help: [
          "Name of this type.",
          '<span class="error-text">Choose this wisely because it will not be modifiable in the future.</span>',
          "Do not use a name that already exist, or you will have unexpected behaviours.",
          "Names starting and ending with underscores (e.g. _something_) are reserved and cannot be used\nthe following names are also reserved: "+ datoTypes.join(", ") +".",
        ],
      },

      {
        inputPreset: "group",
        help: "The group this type belongs to."
      },

      //
      //                              DISPLAY

      {
        key: "display",
        label: "DISPLAY",
        labelLayout: "intertitle",
        help: [
          "All the options that will rule the display of this entry.",
          "(How to figure out it's title, subtitle, color, icon...)",
        ],
        inputifyType: "entry",
        entryType: "_entryDisplay_",
      },

      //
      //                              ACTIONS

      {
        key: "action",
        label: "ACTIONS",
        labelLayout: "intertitle",
        help: "What to do when the user is requesting the following actions on an entry of this type.",
        inputifyType: "entry",
        isAdvanced: true,
        object: {
          structure: [
            {
              key: "edit",
              help: "Modify the default behaviour when user asks to edit an entry\n(default behaviour is to open edition window).",
              inputPreset: 'zeusMethod(action.edit)',
            },
            {
              key: "view",
              help: "Modify the default behaviour when user asks to view an entry\n(default behaviour is to open view window).",
              inputPreset: 'zeusMethod(action.view)',
            },
            {
              key: "storeInHash",
              help: "Actions that should be stored in url hash.",
              inputifyType: "tickbox",
              // prepareValue: ["edit", "view"],
              choices: ["edit", "view"],
            },
          ],
        },
      },

      //
      //                              EVENTS

      {
        key: "event",
        label: "EVENTS",
        labelLayout: "intertitle",
        help: "Custom actions to execute when some things happen to entries of this type.",
        inputifyType: "entry",
        isAdvanced: true,
        object: {
          structure: [
            {
              key: "saved",
              help: "A callback that will be executed if an entry of this type has been saved.",
              inputPreset: 'zeusMethod(event.saved)',
            },
            {
              key: "removed",
              help: "A callback that will be executed if an entry of this type has been removed.",
              inputPreset: 'zeusMethod(event.removed)',
            },
            {
              key: "moved",
              help: "A callback that will be executed if an entry of this type has been moved.",
              inputPreset: 'zeusMethod(event.moved)',
            },
            {
              key: "changed",
              help: "A callback that will be executed when an entry of this type has been modified (moved, removed or saved).",
              inputPreset: 'zeusMethod(event.changed)',
            },
          ],
        },
      },

      //
      //                              EDIT

      {
        key: "edit",
        label: "EDIT",
        labelLayout: "intertitle",
        help: [
          "All the options that will rule the edition of this entry.",
          "(The list of inputs allowing to edit it...)",
        ],
        inputifyType: "entry",
        entryType: "_edify_",
      },

      //
      //                              VIEW

      {
        key: "view",
        label: "VIEW",
        labelLayout: "intertitle",
        help: [
          "All the options that will rule how this entry is displayed, when it's view is opened with the default view.",
          "Those options will probably be ignored if you've set a custom behaviour in the upper 'open' option."
        ],
        inputifyType: "entry",
        entryType: "_viewify_",
      },

      //                              ¬
      //

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
