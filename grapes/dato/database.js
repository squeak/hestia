var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var PouchDB = require("pouchdb");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "database",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  DISPLAY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {
    creationMenu: { icon: "database", },
    title: 'zeus+display.title.anyValue(["display.title", "name"])',
    subtitle: 'zeus+display.title.joined("availableTypes")',
    // color: 'zeus+display.icon.valueOrResult("display.color", "#11F1E4")',
    color: 'zeus+display.icon.valueOrResult("display.color")',
    icon: 'zeus+display.icon.valueOrResult("display.icon", "database")',
    image: 'zeus+display.image.imageAttachedToEntry("display.thumbnail")',
    additional: {
      secondaryColor: 'zeus+display.icon.result("#11F1E4")',
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.openUrl("/#db=%%name%%")',
    storeInHash: ["edit"], // do not store in hash the fact that they are viewed, otherwise it tries to open them again when accessed to page via "previous" button
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EVENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  event: {
    saved: function (dbConfig) {
      var tablifyApp = this;

      // check if this db is currently open in dato
      var targetDatobase = _.findWhere(tablifyApp.allDatobases, { originalDatabaseName: dbConfig.name, });
      if (!targetDatobase) return;
      var targetTablified = targetDatobase.tablifyApp;

      // ask if user want to refresh opened database now
      uify.menu({
        title: "The database you edited is opened.<br>If you want to make use of the last changes you made, you will have to refresh it.<br>What do you want to do?",
        buttons: [
          {
            title: "Just reload the database (default).<br>⚠️ This will close all entries currently being viewed or edited.",
            key: "enter",
            click: function () {
              targetTablified.datobject.reloadDatabase(null, true);
            },
          },
          {
            title: "Reload the database and <u>v</u>isit it.<br>⚠️ This will close all entries currently being viewed or edited.",
            key: "v",
            click: function () {
              targetTablified.datobject.reloadDatabase();
            },
          },
          {
            title: "Do nothing, I'll reload manually if I think it's necessary.",
            key: "esc",
          },
        ],
      });

    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {

    // CUSTOM TITLE IN POSTIFY
    title: function (entry) {
      if (entry) return "database — "+ entry.name
      else return "new database";
    },

    // MAKE SURE USER REALLY KNOW WHAT THEY ARE DOING
    removeEntryWarningContent: function (entryBefore) {
      return [
        "Remove database '"+ entryBefore.name +"'?",
        "All entries you've stored in this database will be lost.",
        "⚠️ This action is not reversible! ⚠️",
        "",
        "To confirm removing this database, please enter it's name here:",
      ].join("<br>");
    },
    removeEntryWarningValidationPrompt: function (enteredText, entryBefore) {
      if (enteredText != entryBefore.name) {
        if (enteredText) uify.toast.warning("The name you passed '"+ enteredText +"' is different than database name '"+ entryBefore.name +"'.")
        else uify.toast.warning("You should enter the database name if you want to confirm removal.");
        return false
      }
      else return true;
    },

    // REMOVE POUCH AND CLOSE DB BEFORE REMOVING DB
    beforeRemove: function (entryBefore, callback) {
      var postified = this;

      // if db is opened in dato, close it
      var targetDatobase = _.findWhere(postified.storage.tablifyApp.allDatobases, { originalDatabaseName: entryBefore.name, });
      if (targetDatobase) targetDatobase.close();

      // remove pouch
      var currentUser = page.authenticatedUser ? page.authenticatedUser.name : "_visitor_";
      var pouchToDestroy = new PouchDB(currentUser +"--"+ entryBefore.name);
      pouchToDestroy.destroy().then(function () {
        uify.toast("Successfully detroyed '"+ entryBefore.name +"' database.");
        callback();
      }).catch(function (err) {
        uify.toast.error("Failed to destroy '"+ entryBefore.name +"' database.<br>See logs for more details.");
        $$.log.detailedError(
          "hestia/grapes/dato/database",
          "Failed to detroy pouch database.",
          err
        );
      });
      delete pouchToDestroy;

    },

    // STRUCTURE
    structure: [

      //
      //                              NAME AND GROUP

      {
        key: "name",
        label: "DATABASE NAME",
        labelLayout: "intertitle",
        inputifyType: "normal",
        locked: function () { return !this.storageRecursive("isNewEntry"); },
        valueOutVerify: function (val) {
          var inputified = this;
          if (!val) throw "you must choose a database name"
          else if (!val.match(/^[a-z]+[0-9a-z\-\_\(\)\+\$]*$/)) throw "invalid name for database"
          else if (val == "dato") throw "'dato' is a reserved database name, you cannot use it"
          else return val;
        },
        help: [
          "Name of this database.",
          '<span class="error-text">Choose this wisely because it will not be modifiable in the future.</span>',
          "Must start with a letter.",
          "Can only contain :\n - lowercase characters (a-z)\n - digits (0-9)\n - any of the following characters _$()+-",
        ],
        mandatory: true,
      },

      {
        inputPreset: "group",
        help: "The group this database belongs to."
      },

      //
      //                              DISPLAY

      {
        key: "display",
        labelLayout: "intertitle",
        help: "Options related to how this database and it's content will be displayed.",
        inputifyType: "entry",
        // inputifyType: "object",
        entryType: "_display_",
      },

      //
      //                              DATABASE OPTIONS

      { key: "DATABASE OPTIONS", inputifyType: "intertitle", },

      {
        key: "availableTypes",
        inputPreset: "availableTypes",
        inputifyType: "suggest",
        help: "Choose here the list of type of entries that can be created in your database.",
      },

      {
        key: "pathsDatabase",
        inputifyType: "boolean",
        help: [
          "If you set this to true, entries in this database will be organizable in different folders.",
          '<span class="warning-text">⚠️ If you set this on after having already created entries in this database, the entries created before setting this on will not have any path set. You will have to manually open the list of entries with unreachable path and give those entries a path (you can open that view clicking on <span class="icon-chain-broken"></span> icon when in this database home).</span>'
        ],
      },

      {
        inputPreset: "buttons_tablifyToolbar",
        isAdvanced: true,
        help: "A list of custom buttons to display in toolbar.",
      },

      {
        key: "disableEditing",
        inputifyType: "boolean",
        isAdvanced: true,
        help: "If true, the entries of the database won't be editable, and you will not be able to add new ones as well.",
      },

      {
        key: "plugins",
        label: "onStart",
        inputifyType: "array",
        isAdvanced: true,
        help: "Run some custom code before starting the database.",
        object: {
          model: {
            presetWithoutKey: true,
            inputPreset: 'zeusMethod(db.onStart)',
          },
        },
      },

      {
        key: "recurringEvents",
        inputifyType: "array",
        isAdvanced: true,
        help: "Setup some notifications or recurring events for this database.",
        object: {
          model: {
            presetWithoutKey: true,
            inputPreset: 'zeusMethod(db.recurringEvents)',
          },
        },
      },

      {
        key: "graph",
        help: "Setup some graphs to display in stats panel.",
        inputifyType: "entry",
        isAdvanced: true,
        entryType: "_graphify_",
      },

      {
        key: "hidden",
        help: "Hide database from view (to toggle display of hidden databases, one must use shift+u keyboard shortcut).",
        inputifyType: "boolean",
        isAdvanced: true,
      },

      //
      //                              REMOTE SYNCHRONIZATION

      {
        key: "remotes",
        inputifyType: "entry",
        labelLayout: "intertitle",
        help: [
          "You can choose here a list of remote databases with which this local one will be synchronized.",
          "This is a good way to have backups of your database, or to synchronize it between devices.",
        ],
        object: {
          shape: "array",
          model: {
            inputifyType: "object",
            object: {
              structure: [

                // REMOTE URL
                {
                  key: "url",
                  inputifyType: "url",
                  labelLayout: "hidden",
                  help: "Url to an existing remote couch database (for example: 'https://mycouch.tld/mydb/').",
                  input: {
                    placeholder: "url to a remote couch database",
                    autocomplete: "new-password",
                  },
                },

                // USERNAME AND PASSWORD
                { key: "usernameAndPasswordIntertitle", label: "USERNAME AND PASSWORD", type: "intertitle", isAdvanced: true, },
                {
                  key: "username",
                  type: "normal",
                  help: [
                    "If this remote database is not public, you should fill here the name of a user on this couch server that has access to this database.",
                    "<span class=\"error-text\">⚠️ Filling your credentials here, means that they will be saved in your home database!",
                    "If you consider those credentials sensitive, you should leave this empty.",
                    "Then when you want to synchronize your database, open it and click the <span class=\"icon-loop2\"></span> button to open the 'synchronization statuses' panel. In that panel, click '<span class=\"icon-key\"></span> unlock this remote manually' and enter your credentials for that remote server.</span>",
                  ],
                  labelLayout: "hidden",
                  isAdvanced: true,
                  input: {
                    placeholder: "username in the remote couchdb server",
                    autocomplete: "new-password",
                  },
                },
                {
                  key: "password",
                  type: "password",
                  help: [
                    "If this remote database is not public, fill here the password for the chosen user.",
                    "<span class=\"error-text\">⚠️ Filling your credentials here, means that they will be saved in your home database!",
                    "If you consider those credentials sensitive, you should leave this empty.",
                    "Then when you want to synchronize your database, open it and click the <span class=\"icon-loop2\"></span> button to open the 'synchronization statuses' panel. In that panel, click '<span class=\"icon-key\"></span> unlock this remote manually' and enter your credentials for that remote server.</span>",
                  ],
                  labelLayout: "hidden",
                  isAdvanced: true,
                  input: {
                    placeholder: "password for this user",
                    autocomplete: "new-password",
                  },
                },

                // REMOTE FILTER
                {
                  key: "filter",
                  labelLayout: "intertitle",
                  isAdvanced: true,
                  help: [
                    "If you wish not to replicate all entries of the database to the remote, you can apply a filter.",
                    "This means that only entries that match this filter will be synchronized from and to the remote database.",
                  ],
                  inputifyType: "object",

                },

              ],
            },
          },
        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
