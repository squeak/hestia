var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var database_grape = require("./database");
// var display_grape = require("../system/_display_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// clone config type to modify
var databaseHome_grape = $$.clone(database_grape, 99);
databaseHome_grape.name = "database-home";

// filter keys to keep
var keysToKeep = ["display", /* "DATABASE OPTIONS", "pathsDatabase",*/ "remotes"];
databaseHome_grape.edit.structure = _.filter(databaseHome_grape.edit.structure, function (inputConfig) {
  return _.indexOf(keysToKeep, inputConfig.key) != -1;
});

// NOTE: if one day readd display, needs to create a _display_databaseHome_ custom type
// var displayOpts = _.findWhere(databaseHome_grape.edit.structure, { key: "display", });
// displayOpts.entryType = "_display_databaseHome_";

// make sure tablify will not automatically add name and path choice in edition window
databaseHome_grape.edit.doNotAutoAddPath = true;

// customize title display in postify view (only)
databaseHome_grape.edit.title = function (entry) { return "DATO HOME SETTINGS"; };

// reload page when config has been changed
databaseHome_grape.event.saved = databaseHome_grape.event.removed = function () {
  var tablifyApp = this;
  uify.confirm({
    type: "warning",
    content: "To see your modifications, the database should be reloaded.<br>Reload now?<br>(If you were editing something and you didn't save, you will lose your modifications.)",
    ok: function () {
      tablifyApp.datobject.reloadDatabase();
    },
  });
};

// remove destroying db when removing entry
delete databaseHome_grape.edit.beforeRemove;
delete databaseHome_grape.edit.removeEntryWarningContent;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = databaseHome_grape;
