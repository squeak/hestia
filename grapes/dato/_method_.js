var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/string");
var descartesCustomMethods_availableCategories = require("descartes/customMethod/categories");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SQUYNTAXIFY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: turn a squyntax function documentation into a string to display it
  ARGUMENTS: ({
    ?description <string|string[]>,
    ?arguments <squyntaxObject[]>,
    ?context: <squyntaxObject>,
    ?return: <squyntaxObject>,
  })
  RETURN: <htmlString>
*/
function squyntaxifyFunction (doc) {

  var resultString = [];
  resultString.push('<span class="hestia-squyntax-function">')

  // DESCRIPTION
  if (doc.description) {
    resultString.push("<b>DESCRIPTION:</b>")
    resultString.push(doc.description);
    resultString.push(" ")
  };

  // ARGUMENTS
  if (doc.arguments) {
    resultString.push("<b>ARGUMENTS: (</b>");
    _.each(doc.arguments, function (docArg) {
      resultString.push(squyntaxify(docArg));
    });
    resultString.push("<b>)</b>");
    resultString.push(" ")
  };

  // CONTEXT
  if (doc.context) {
    resultString.push("<b>CONTEXT</b>:")
    resultString.push(squyntaxify(doc.context));
    resultString.push(" ")
  };

  // RETURN
  if (doc.return) {
    resultString.push("<b>RETURN</b>:")
    resultString.push(squyntaxify(doc.return));
    resultString.push(" ")
  };

  // return result string
  resultString.push("</span>");
  return _.flatten(resultString).join("<br>");

};

/**
  DESCRIPTION: turn a method object into a squyntax description
  ARGUMENTS: ({
    !name: <string>,
    ?type: <string|string[]>,
    ?description: <string|string[]>,
  })
  RETURN: <htmlString>
*/
function squyntaxify (method) {
  var resultString = [];
  if (method.name) resultString.push(method.name +":");
  if (method.type) resultString.push('<span class="squyntax squyntax-type">&lt;'+ simpleOrMultiline(method.type).replace(/\n/g, "<br>").replace(/\s\s/g, "&nbsp;&nbsp;") +'&gt;</span>');
  if (method.description) resultString.push('<span class="squyntax squyntax-description">«'+ simpleOrMultiline(method.description).replace(/\n/g, "<br>").replace(/\s\s/g, "&nbsp;&nbsp;") +'»</span>');
  return resultString.join("<br>");
};

function simpleOrMultiline (stringOrArray) {
  var asArray = _.isArray(stringOrArray) ? stringOrArray : [stringOrArray];
  var escapedArrayOfStrings = _.map(asArray, function (string) { return $$.string.escapeHtml(string); });
  return escapedArrayOfStrings > 1 ? "<br>"+ escapedArrayOfStrings.join("<br>") +"<br>" : " "+ escapedArrayOfStrings[0] +" ";
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_method_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  display: {

    creationMenu: {
      name: "method",
      icon: "code",
      key: "m",
    },

    title: 'zeus+display.title.valueOrResult("name")',
    icon: 'zeus+display.icon.result("code")',
    additional: {
      secondaryColor: 'zeus+display.icon.result("#0eb242")',
    },

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ACTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  action: {
    view: 'zeus+action.view.edit',
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [

      //
      //                              NAME AND GROUP

      {
        key: "name",
        inputifyType: "normal", // TODO: should not allow to make two methods with same name
        label: "METHOD NAME",
        labelLayout: "intertitle",
        mandatory: true,
        help: [
          "Name of this method.",
          '<span class="error-text">Choose this wisely because it will not be modifiable in the future.</span>',
          "Should contain only letters, numbers, underscores (_), and cannot start with a number.",
        ],
        locked: function () { return !this.storageRecursive("isNewEntry"); },
        valueOutVerify: function (val) {
          if (!val || !_.isString(val)) throw "you must choose a name for this method"
          else if (val.match(/\W/) || val.match(/\d/)) throw "the name can only contain letters, numbers, underscores (_), and should not start with a number";
          return val;
        },
      },

      {
        inputPreset: "group",
        help: "The group this method belongs to."
      },

      //
      //                              CATEGORY

      {
        key: "category",
        inputifyType: "select",
        labelLayout: "intertitle",
        mandatory: true,
        choices: _.map(descartesCustomMethods_availableCategories, function (categorySettings) {
          return {
            _isChoice: true,
            label: [
              "<span>"+ categorySettings.categoryName,
              "<small>"+ categorySettings.description +"</small></span>",
            ].join("<br>"),
            value: categorySettings.categoryName,
          };
        }),
        modified: 'zeus+inputify.modified.doOnSiblings("remake", ["code", "CODE_EXPLANATION"])',
      },

      //
      //                              CODE

      {
        key: "code",
        inputifyType: "textarea",
        labelLayout: "intertitle",
        monospace: true,
        mandatory: true,
        cssInput: {
          height: 300,
          fontSize: "0.8em",
        },
        attributes: {
          spellcheck: false,
        },
        condition: 'zeus+inputify.condition.siblingValue("category", { "$exists": true }, true)',
      },

      //
      //                              CODE WRAPPING

      {
        key: "CODE_EXPLANATION",
        inputifyType: "uneditable",
        label: "How to fill this code:",
        labelLayout: "stacked",
        condition: 'zeus+inputify.condition.siblingValue("category", { "$exists": true }, true)',
        monospace: true,
        value: function () {

          var inputified = this;
          var categoryName = inputified.getSiblingValue("category");
          var methodCategorySettings =_.findWhere(descartesCustomMethods_availableCategories, { categoryName: categoryName, });
          if (!methodCategorySettings) return;
          var argumentList = $$.array.merge("additionalArguments", _.pluck(methodCategorySettings.arguments, "name"));

          var includedLibraries = {
            "$ (yquerj)": "https://squeak.eauchat.org/libs/yquerj",
            "_ (underscore)": "https://underscorejs.org/",
            "$$ (squeak, including all it's extensions)": "https://squeak.eauchat.org/libs/squeak",
            "async": "https://caolan.github.io/async/v3/",
            "uify (including it's extensions)": "https://squeak.eauchat.org/libs/uify",
            "descartesUtils": "https://squeak.eauchat.org/libs/descartes?utilities",
            // "dialogify": "https://squeak.eauchat.org/libs/dialogify",
            "markdownify": "https://squeak.eauchat.org/libs/markdownify",
          };

          return squyntaxifyFunction({

            description: [
              'The previous code will be evaluated wrapped in a function looking like this: <span class="function-description">function ('+ argumentList.join(", ") +') { /* YOUR CODE */ }</span>',
              'The following libraries are accessible and can be used in the method: '+ _.map(includedLibraries, function (href, name) { return '<a href="'+ href +'">'+ name +'</a>' }).join(", "),
            ],

            // ARGUMENTS
            arguments: _.flatten([
              {
                name: "additionalArguments",
                type: "{ [<string>]: <any> }",
                description: "an object containing additional values that can be configured when this method is selected",
              },
              methodCategorySettings.arguments,
            ], true),

            // CONTEXT AND RETURN
            context: methodCategorySettings.context,
            return: methodCategorySettings.return,

          });

        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
