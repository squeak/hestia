
module.exports = [

  // SYSTEM CONFIGS (SUBENTRIES)
  require("./system/_button_edify_"),
  require("./system/_button_tablifyEntry_"),
  require("./system/_button_tablifyToolbar_"),
  require("./system/_button_viewify_"),
  require("./system/_column_"),
  require("./system/_display_"),
  require("./system/_edify_"),
  require("./system/_entryDisplay_"),
  require("./system/_graphify_"),
  require("./system/_inputify_"),
  require("./system/_inputify_withOptionalKey_"),
  require("./system/_inputify_withoutKey_"),
  require("./system/_viewify_"),
  require("./system/_viewify-field_"),

  // DATO
  require("./dato/database"),
  require("./dato/database_home"),
  require("./dato/default"),
  require("./dato/folder"),
  require("./dato/type"),
  require("./dato/_method_"),
  require("./dato/_data_"),

  // BASIC CONFIGS
  require("./basics/bookmark"),
  require("./basics/contact"),
  require("./basics/link"),
  require("./basics/location"),
  require("./basics/monitoring"),
  require("./basics/note"),
  require("./basics/separator"),
  require("./basics/subject"),
  require("./basics/todo"),

];
