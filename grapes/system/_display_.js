var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_display_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      //
      //                              COSMETICS

      { key: "APPEARANCE", inputifyType: "intertitle", isAdvanced: true, },
      {
        key: "title",
        isAdvanced: true,
        help: "The title displayed in the bottom-left corner, when you open the database/folder.",
        inputifyType: "normal",
      },
      {
        key: "color",
        help: "Color for this database/folder.",
        inputifyType: "color",
      },
      {
        key: "icon",
        help: "Icon for this database/folder.",
        inputifyType: "icon",
      },
      {
        key: "thumbnail",
        help: "A thumbnail for this entry in homepage's list.",
        inputifyType: "file",
        file: {
          process: [ 'zeus+inputify.fileProcess.squareCrop()', 'zeus+inputify.fileProcess.resizeImage(400, 400)' ],
        },
      },
      {
        key: "background",
        help: "Background for this database/folder.",
        inputifyType: "file",
        file: {
          process: [ 'zeus+inputify.fileProcess.resizeImage(1540, 1540, "jpeg", 0.8)' ],
        },
      },
      {
        inputPreset: "cssRules",
        help: [
          "Global rules you want to apply to this table/folder display.",
          "Each key of this object, is the css selector to use.",
          "This selector will be prepended by the class of this tablify container, so the styles can only be applied to it's children elements, or to itself using & like lesscss does.",
        ],
      },

      //
      //                              LAYOUT

      { key: "LAYOUT", inputifyType: "intertitle", isAdvanced: true, },
      {
        key: "layouts",
        isAdvanced: true,
        help: "The different layouts that can be chosen to display this database/folder (the first one of the list will be the default one).",
        inputPreset: 'zeusMethod(tablify.layouts)',
      },
      // {
      //   key: "layoutSize",
      //   inputifyType: "radio",
      //   isAdvanced: true,
      //   // condition: 'zeus+inputify.condition.siblingValue("layouts", { "$in": ["gallery", "list", "free"] }, true)',
      //   help: "The size to give to the entries when displaying them in the list.",
      //   choices: ["S", "M", "L"],
      // },
      {
        key: "columns",
        inputifyType: "entry",
        isAdvanced: true,
        // condition: 'zeus+inputify.condition.siblingValue("layouts", { "$in": ["table"] }, true)',
        help: "The list of columns that will be displayed in table view.",
        object: {
          shape: "array",
          sortDisplay: "key",
          model: {
            inputifyType: "entry",
            entryType: "_column_",
          },
        },
        siblingsSubscriptions: [
          {
            key: "layouts",
            event: "modified",
            action: "remake",
          },
        ],
      },

      //
      //                              ORGANIZATION

      { key: "ORGANIZATION", inputifyType: "intertitle", isAdvanced: true, },
      {
        key: "grouping",
        isAdvanced: true,
        help: "Group entries when displaying the list.",
        inputPreset: 'zeusMethod(tablify.grouping)',
      },
      {
        key: "groupingCustomDisplay",
        isAdvanced: true,
        help: "Use a custom method to display groups intertitles content.",
        inputPreset: 'zeusMethod(tablify.groupingCustomDisplay)',
      },
      {
        key: "sorting",
        isAdvanced: true,
        help: "Use a custom sorting to order entries displayed (if entries are also grouped, the grouping is applied first, and then this sorting is applied inside each group, while groups are sorted alphabetically).",
        inputPreset: 'zeusMethod(tablify.sorting)',
      },
      {
        key: "sortReverse",
        isAdvanced: true,
        inputifyType: "boolean",
        help: "Reverse sorting order.",
      },
      {
        key: "searching",
        help: "You may use this option to improve the default searching functionnality, the function chosen here will return a string containing additional info to improve entries matching when the user searches/filters the database.",
        inputPreset: 'zeusMethod(tablify.searching)',
        isAdvanced: true,
      },
      // { ////// TODO TODO TODO
      //   key: "filter",
      //   inputifyType: "TODO",
      //   help: "only the entries that pass this filter will be shown in database listing",
      // },
      {
        key: "limit",
        isAdvanced: true,
        help: [
          "Number of database entries loaded in the page (0 means it will show all entries).",
          "If the database contains a lot of entries, it's better to set something here, otherwise the database may take quite some time to load every time you open it.",
        ],
        inputifyType: "radio",
        choices: [0, 10, 50, 100, 200, 500, 1000],
      },
      {
        key: "rememberLimit",
        isAdvanced: true,
        help: "When limit is changed in the interface, do you want the last chosen setup to be remembered?",
        inputifyType: "boolean",
      },

      //
      //                              ADVANCED

      { key: "ADVANCED", inputifyType: "intertitle", isAdvanced: true, },
      {
        key: "bodyHeader",
        help: "Some message to display above the list of db entries.",
        inputPreset: 'zeusMethod(display.bodyHeader, "textarea")',
        isAdvanced: true,
      },

      //                              ¬
      //

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
