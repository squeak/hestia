
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_column_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      //
      //                              GENERAL

      { key: "GENERAL", inputifyType: "intertitle", },

      {
        key: "key",
        help: "The key of this column.",
        inputifyType: "normal",
      },

      {
        key: "label",
        help: "A custom label to display in this column's header.",
        inputifyType: "normal", // TODO, maybe could be a function, but not even sure tablify supports it
      },

      {
        key: "class",
        help: "A custom class to apply to this column's cells.",
        inputifyType: "normal", // TODO, could also be a function, tablify supports: function(model<backboneModel>):string
      },

      //
      //                              SORT

      { key: "SORT", inputifyType: "intertitle", },
      {
        key: "sorting",
        help: "Use a custom sorting to order entries displayed when this column is selected.",
        inputPreset: 'zeusMethod(tablify.sorting)',
      },
      {
        key: "sortReverse",
        help: "If true, will reverse the sorting order.",
        inputifyType: "boolean",
      },
      {
        key: "grouping",
        help: "Group entries when displaying the list sorted by this column.",
        inputPreset: 'zeusMethod(tablify.grouping)',
      },
      {
        key: "groupingCustomDisplay",
        help: "Use a custom method to display groups intertitles content.",
        inputPreset: 'zeusMethod(tablify.groupingCustomDisplay)',
      },
      {
        key: "searching",
        help: "You may use this option to improve the default searching functionnality, the function chosen here will return a string containing additional info to improve entries matching when the user searches/filters the database.",
        inputPreset: 'zeusMethod(tablify.searching)',
        isAdvanced: true,
      },

      //
      //                              DISPLAY AND ACTIONS

      { key: "DISPLAY AND ACTIONS", inputifyType: "intertitle", },

      // CSS PROPERTIES
      {
        key: "cssProperties",
        inputifyType: "textarea",
        help: [
          "Properties you want to apply to this column cells.",
          "e.g. width: 20px; background-color: red; ...",
        ],
        monospace: true,
      },

      // DISPLAY FUNCTION
      {
        key: "display",
        help: [
          "Customize the display of this column's entries.",
          "There are two ways to use this function:",
          "  - return with it what you want to be assigned as html in $cell",
          "  - do not return and do whatever change you want to $cell (if you return something, the return may override the other changes you made)",
        ],
        inputPreset: 'zeusMethod(tablify.cellDisplay)',
      },

      // EVENTS CALLBACKS
      {
        key: "events",
        help: "Events fired when something happens to one this column's cell.",
        inputifyType: "object",
        isAdvanced: true,
        object: {
          model: {
            inputifyType: "TODO", // TODO: events: <{ [eventName]: <eventFunc(event)this=Entry> }>
          },
        },
      },

      // CLICK CALLBACK
      {
        key: "click",
        help: [
          "Fired when cell is clicked.",
          "(redundant with events options before, but is here for convenience (it has more arguments passed))",
        ],
        inputifyType: "TODO", // TODO: <function( model<backboneModel>, $cell<yquerjObject>, event)this=Entry>
        isAdvanced: true,
      },

      // RENDER CALLBACK
      {
        key: "renderCallback",
        help: "Executed when a cell is rendered.",
        inputifyType: "TODO", // TODO: <function(model<backboneModel>, $cell<yquerjObject>)this=Entry>
        isAdvanced: true,
      },

      //                              ¬
      //

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
