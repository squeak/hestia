
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_graphify_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: "&lt;LIST OF GRAPHS&gt;<br>here you can setup graphs to display in the stats panel",
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      {
        key: "graphs",
        labelLayout: "hidden",
        help: "Setup some graphs to display in stats panel.",
        inputifyType: "array",
        object: {
          model: {
            inputifyType: "entry",
            object: {
              structure: [

                //
                //                              NAME AND TITLE

                {
                  key: "name",
                  type: "normal",
                  labelLayout: "hidden",
                  mandatory: true,
                  help: "The name of this graph, to display on the clickable tab title (better to keep it short).",
                  input: { placeholder: "Name of this graph", },
                },

                {
                  key: "title",
                  type: "normal",
                  isAdvanced: true,
                  labelLayout: "hidden",
                  input: { placeholder: "Title/description of this graph", },
                  help: "Title of this graph, displayed when graph is shown (can be longer).",
                },

                //
                //                              FILTERING ENTRIES BEFORE DATA EXTRACTION

                {
                  key: "filterEntries",
                  help: "Filter out some entries to omit them when extracting data to graph.",
                  labelLayout: "intertitle",
                  isAdvanced: true,
                  inputPreset: 'zeusMethod(graphify.filterEntries)',
                },

                //
                //                              EXTRACTING DATA TO GRAPH

                {
                  key: "dataExtraction",
                  mandatory: true,
                  labelLayout: "intertitle",
                  help: "The method to extract from database the data that should be graphed.",
                  inputPreset: 'zeusMethod(graphify.dataExtraction)',
                },

                //
                //                              GROUPING, FILTERING AND SORTING COUNTS

                { key: "GROUPING SORTING AND FILTERING COUNTS", type: "intertitle", },

                {
                  key: "groupCounts",
                  help: "Group counts in categories to graph.",
                  inputPreset: 'zeusMethod(graphify.groupCounts)',
                  isAdvanced: true,
                },
                {
                  key: "filterCounts",
                  help: "Filter out some counts to omit them in graph.",
                  inputPreset: 'zeusMethod(graphify.filterCounts)',
                  isAdvanced: true,
                },
                {
                  key: "sortCounts",
                  help: "How to order display counts in the graph.",
                  inputPreset: 'zeusMethod(graphify.sortCounts)',
                },
                {
                  key: "sortReverse",
                  inputifyType: "boolean",
                  help: "Reverse chosen sorting.",
                },

                //
                //                              GENERAL STYLES

                { key: "GRAPH GENERAL STYLES", type: "intertitle", },

                {
                  key: "type",
                  type: "select",
                  help: "The type of graph you want to display.",
                  choices: ["bar", "horizontalBar", "line", "radar", "pie", "doughnut", "polarArea", "bubble", "scatter"],
                },
                {
                  key: "colorMaker",
                  help: "How to color graphs lines, bars...",
                  inputPreset: 'zeusMethod(graphify.colorMaker)',
                },
                {
                  key: "usePatterns",
                  inputifyType: "boolean",
                  help: "Whether to use patterns to color bars and pie surfaces or not.",
                },
                {
                  key: "labelDisplay",
                  help: "Customize the display of labels.",
                  isAdvanced: true,
                  inputPreset: 'zeusMethod(graphify.labelDisplay)',
                },

                //
                //                              CUSTOMIZE DATASETS

                {
                  labelLayout: "intertitle",
                  label: "CUSTOMIZE DATASETS SPECIFICALLY",
                  key: "datasetsOptions",
                  inputifyType: "entry",
                  isAdvanced: true,
                  help: "Customize options for the dataset generated by your data.",
                  object: {
                    help: "You can add here custom options for the datasets generated by your data.<br>Click the plus, enter your dataset name, and then the custom options you wish to use.",
                    model: {
                      inputifyType: "object",
                      labelLayout: "stacked",
                      object: {
                        structure: [
                          {
                            key: "type",
                            type: "select",
                            help: "The type of graph you want to use for this dataset.",
                            choices: ["bar", "horizontalBar", "line", "radar", "pie", "doughnut", "polarArea", "bubble", "scatter"],
                          },
                          {
                            key: "label",
                            type: "normal",
                            help: "Customize the label for this dataset.",
                          },
                          {
                            key: "colorMaker",
                            help: "How to color this dataset lines, bars...",
                            inputPreset: 'zeusMethod(graphify.colorMaker)',
                          },
                          {
                            key: "xAxisID",
                            inputifyType: "normal",
                            help: "Id of the x axis to use for this dataset.",
                          },
                          {
                            key: "yAxisID",
                            inputifyType: "normal",
                            help: "Id of the x axis to use for this dataset.",
                          },
                          {
                            key: "usePatterns",
                            inputifyType: "boolean",
                            help: "Whether to use patterns to color bars and pie surfaces or not.",
                          },
                        ],
                      },
                    },
                  },
                },

                //
                //                              CUSTOMIZE AXES

                {
                  labelLayout: "intertitle",
                  label: "CUSTOMIZE AXES",
                  key: "axesOptions",
                  inputifyType: "entry",
                  isAdvanced: true,
                  help: "Custom options to use for the given x or y axes used in datasets.",
                  object: {
                    help: "You can add here custom options for the axes used by your data sets.<br>Click the plus, enter the id of the axis you want to customize, and then the custom options you wish to use.<br>If you didn't specify any axis id, it should be defaultX for an x axis and defaultY for a y axis.",
                    model: {
                      inputifyType: "object",
                      labelLayout: "stacked",
                      object: {
                        structure: [
                          {
                            key: "stacked",
                            inputifyType: "boolean",
                            help: "Should data be stacked in this axis, or not.",
                          },
                          {
                            key: "position",
                            inputifyType: "radio",
                            help: "Position of this axis compared to the graph.",
                            choices: ["left", "right", "top", "bottom"],
                          },
                        ],
                      },
                    },
                  },
                },

                //
                //                              CUSTOM CHART.JS OPTIONS

                {
                  key: "chartjsOptions",
                  type: "object",
                  isAdvanced: true,
                  labelLayout: "intertitle",
                  help: "Any additionl chart.js options to set.",
                },

                //                              ¬
                //

              ],
            },
          },
          sortDisplay: function ($li, entry) {
            return $li.span({
              text: entry.name || "MISSING NAME",
            });
          },
        },
      },

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
