
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_entryDisplay_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: "&lt;ENTRY DISPLAY SETTINGS&gt;<br>here you can setup how entries of this type will be displayed, what should be their title, subtitle, color, icon...",
    structure: [

      //
      //                              TYPE DISPLAY WHEN CREATING AN ENTRY

      {
        key: "creationMenu",
        help: "Customize display of this type in entry creation menu.",
        inputifyType: "entry",
        isAdvanced: true,
        labelLayout: "intertitle",
        label: "DISPLAY IN ENTRY CREATION MENU",
        object: {
          structure: [
            {
              key: "name",
              help: "The name to display when creating a new entry of this type.",
              inputifyType: "normal",
            },
            {
              key: "icon",
              help: "An icon to display by the name of the type.",
              inputifyType: "icon",
            },
            {
              key: "shortcut",
              help: "A shortcut key to press to select that type in the entry creation menu.",
              inputifyType: "normal",
            },
          ],
        },
      },

      //
      //                              GENERAL

      { key: "GENERAL", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "title",
        help: "Choose here a method that will return the title of your entry.",
        inputPreset: 'zeusMethod(display.title)',
        isAdvanced: false,
      },
      {
        key: "subtitle",
        help: "Choose here a method that will return the subtitle of your entry.",
        inputPreset: 'zeusMethod(display.title)',
        isAdvanced: false,
      },
      {
        key: "color",
        help: "Choose here a method that will return the color of your entry.",
        inputPreset: 'zeusMethod(display.color)',
        isAdvanced: false,
      },
      {
        key: "icon",
        help: "Choose here a method that will return the icon of your entry.",
        inputPreset: 'zeusMethod(display.icon)',
        isAdvanced: false,
      },
      {
        key: "image",
        help: "Choose a method that will return the image of your entry.",
        inputPreset: 'zeusMethod(display.image)',
        isAdvanced: false,
      },

      //
      //                              ADDITIONAL

      {
        key: "additional",
        inputifyType: "entry",
        help: "Additional display options (list view, geo options, size, line style and thickness...).",
        labelLayout: "intertitle",
        isAdvanced: true,
        object: {
          structure: [

            { key: "GENERAL", inputifyType: "intertitle", },
            {
              key: "secondaryColor",
              help: "Choose here a method that will return a secondary color for your entry (this is used to color the title bar in view and edition windows).",
              inputPreset: 'zeusMethod(display.color)',
            },

            { key: "LIST-LIKE VIEW", inputifyType: "intertitle", },
            {
              inputPreset: "buttons_tablifyEntry",
              help: "A list of custom buttons to display in each entry (only in list-like views).",
            },

            { key: "GEO SHAPES", inputifyType: "intertitle", },
            {
              key: "geoKey",
              help: "If this entry should be displayed on a map, at which key are geo coordinates information (and geometry infos) stored in these entries.",
              inputPreset: 'zeusMethod(display.geoKey)',
            },
            {
              key: "weight",
              help: [
                "If this entry is a displayable shape path, how to define the weight of it's stroke (stroke weight is counted in pixels).",
                "If it's a marker/point shape, will determine how big to display it (point weight is counted in tens of pixels).",
              ],
              inputPreset: 'zeusMethod(display.additional)',
            },
            {
              key: "strokeDashArray",
              help: [
                "If this entry is a displayable shape path, how to define the style of it's stroke.",
                "This lets you create complex dash patterns. You can set a single number or multiple ones to make more complex dash patterns (e.g. 5,3,2).",
                "Search stroke-dasharray svg option on internet for more details.",
              ],
              inputPreset: 'zeusMethod(display.additional)',
            },
            {
              key: "fillColor",
              help: "If this entry is a displayable shape path, use this to customize the color of it's filling (by default it would be the same as generally set color).",
              inputPreset: 'zeusMethod(display.additional)',
            },
            {
              key: "fillOpacity",
              help: "If this entry is a displayable shape path, use this to customize the opacity of it's filling.",
              inputPreset: 'zeusMethod(display.additional)',
            },
          ],
        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
