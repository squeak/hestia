var _ = require("underscore");
var $$ = require("squeak");
var buttonMaker = require("../buttonMaker");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// MAKE BUTTON GRAPE
var _button_tablifyToolbar_ = buttonMaker("_button_tablifyToolbar_", "tablify.toolbarButtonClick", "tablify.toolbarButtonCondition");

// ADD POSITION OPTIONS
_button_tablifyToolbar_.edit.structure.unshift(
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // INTERTITLE
  { key: "IN TOOLBAR", inputifyType: "intertitle", },

  // POSITION
  {
    key: "position",
    label: "Position: ",
    inputifyType: "radio",
    help: "Position of button in the toolbar.",
    choices: ["start", "end"],
    defaultValue: "start",
  },

  // POSITIONIZE FIRST
  {
    key: "positionizeFirst",
    label: "Positionize first: ",
    help: "If the button should be positionned before other buttons already displayed or after.",
    inputifyType: "boolean",
  },

  // BUTTON TYPE
  {
    key: "type",
    label: "Button type: ",
    help: "Use a custom preset type of button.",
    inputifyType: "radio",
    choices: [
      { _isChoice: true, value: null, label: "no preset" },
      "separator",
      "blank",
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
);

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = _button_tablifyToolbar_;
