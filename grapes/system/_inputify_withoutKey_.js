var _ = require("underscore");
var $$ = require("squeak");

var inputifyWithoutKey = $$.clone(require("./_inputify_"), 4);
inputifyWithoutKey.name = "_inputify_withoutKey_";
inputifyWithoutKey.edit.structure.splice(1, 1);
delete inputifyWithoutKey.edit.help;

module.exports = inputifyWithoutKey
