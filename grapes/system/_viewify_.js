var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  AUTOFILL VIEW FIELDS FROM EDIT FIELDS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// SET VIEW FIELDS ACCORDING TO EDIT FIELDS
function autofillViewFromEdit (viewInputified, viewInputValue) {
  // viewInputified.entry_dialogInputified is missing ???
  if (!viewInputified.entry_dialogInputified) return uify.alert.error("Failed to autofill view fields from edit keys.");

  // get keys from edit section
  var rootInputified = viewInputified.storageRecursive("rootInputified");
  if (!rootInputified) return uify.alert.error("Failed to autofill view fields from edit ones.<br>Couldn't find root input.");
  var rootInputValue = viewInputified.storageRecursive("rootInputified").get();
  var newViewFields = _.map($$.getValue(rootInputValue, "edit.structure"), function (editFieldInputifyOptions) {
    var field = { deepKey: editFieldInputifyOptions.key, };
    if (editFieldInputifyOptions.label) field.label = editFieldInputifyOptions.label;
    return field;
  });

  // set new fields to inputs
  var newViewInputValue = $$.clone(viewInputValue, 10); // clone object otherwise inputify fails to detect the object has changed since before (in case of canceling modifications afterwards)
  newViewInputValue.fields = newViewFields;
  viewInputified.entry_dialogInputified.set(newViewInputValue);

};

// PROMPT IF VIEW FIELDS SHOULD BE REPLACED BY THE ONES IN EDIT
function autofillViewFromEdit_promptIfNecessary (viewInputified) {
  var viewInputValue = viewInputified.get() || {};
  if (viewInputValue.fields && viewInputValue.fields.length) uify.confirm({
    type: "warning",
    content: "Are you sure you want to autofill view fields from the edit section?<br>This will replace the current fields.",
    ok: function () { autofillViewFromEdit(viewInputified, viewInputValue); },
  })
  else autofillViewFromEdit(viewInputified, viewInputValue);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_viewify_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: function ($helpMessage) {
      var viewInputified = this;

      // DISPLAY HELP MESSAGE
      var $helpMessage = $helpMessage.css({
      }).html("&lt;LIST OF FIELDS&gt;<br>here you can setup the way your entry will be displayed in the view window");

      // ADD A CUSTOM BUTTON
      uify.button({
        $container: $helpMessage,
        inlineTitle: true,
        invertColor: true,
        title: "autofill fields from edit keys",
        class: "hestia-type-viewify-help_button",
        click: function () { autofillViewFromEdit_promptIfNecessary(viewInputified); },
      });

    },
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      //
      //                              FIELDS

      { key: "FIELDS", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "fields",
        labelLayout: "hidden",
        help: "Customize the list of fields to display in view page.",
        inputifyType: "array",
        object: {
          model: {
            inputifyType: "entry",
            entryType: "_viewify-field_",
          },
          sortDisplay: function ($li, entry) {
            return $li.span({
              text: entry.deepKey || "WITHOUT KEY",
            });
          },
        },
      },


      //
      //                              GENERAL

      { key: "GENERAL", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "cssWindow",
        isAdvanced: true,
        labelLayout: "stacked",
        help: "Css styles to apply to the dialog.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "cssContent",
        isAdvanced: true,
        labelLayout: "stacked",
        help: "Css styles to apply to the content container.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "cssRules",
        isAdvanced: true,
        help: [
          "Custom global css rules",
          "⚠⚠⚠ These rules will be applied to the whole page, so be very specific if you don't want to mess everything up.",
          "It is recommended that you prepend all your rules with the prefix of this type of entry (which is '.viewify-tablify--<entryType>')",
        ],
        labelLayout: "stacked",
        inputifyType: "long",
        monospace: true,
      },


      //
      //                              BUTTONS

      {
        label: "BUTTONS",
        labelLayout: "intertitle",
        inputPreset: "buttons_viewify",
        isAdvanced: true,
        help: "A list of custom buttons to display in the menu bar.",
      },

      //                              ¬
      //

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
