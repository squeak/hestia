var buttonMaker = require("../buttonMaker");

var _button_tablifyEntry_ =  buttonMaker("_button_tablifyEntry_", "display.buttonClick", "display.buttonCondition");
// remove key input (shortcut can't be used on entries buttons (because there are plenty of buttons in the same page, how could you know which one the shortcut triggers))
_button_tablifyEntry_.edit.structure = _.reject(_button_tablifyEntry_.edit.structure, function (obj) {
  return obj.key === "key";
});

module.exports = _button_tablifyEntry_;
