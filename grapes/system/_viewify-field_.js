
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_viewify-field_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: "&lt;FIELD&gt;<br>here you can setup a field to display in the view window",
    structure: [

      //
      //                              KEY, DISPLAY...

      {
        key: "deepKey",
        inputifyType: "simple",
        label: "deep key",
        // labelLayout: "hidden",
        // input: { placeholder: "key or deep key", },
        help: [
          "DeepKey of the value to show.",
          "(You may omit this if you don't use showKey and set a display function that doesn't use deepKey.)",
        ],
        choices: function (callback) {
          var inputified = this;

          var rootInputified = inputified.storageRecursive("rootInputified");
          var valueOrValuesToSuggest = [];

          // EXTRACT DEEP KEYS RECURSIVELY
          function extractDeepKeysRecursively (obj, prefixToKey) {
            if (!obj || !obj.structure || !_.isArray(obj.structure)) return;
            _.each(obj.structure, function (subobj) {
              if (subobj && (subobj.key || subobj.key === 0)) {
                valueOrValuesToSuggest.push(prefixToKey + subobj.key);
                extractDeepKeysRecursively(subobj.object, prefixToKey + subobj.key +".");
              };
            });
          };

          // EXTRACT VALUES FROM INPUTIFIED
          if (rootInputified && _.isFunction(rootInputified.get)) {
            var inputValue = rootInputified.get();
            if (inputValue) extractDeepKeysRecursively(inputValue.edit, "");
          }
          // COULD NOT FIGURE OUT INPUTIFIED
          else $$.log.error(
            "Could not find the root input of for this postify window.",
            "inputified:", inputified,
            "root inputified:", rootInputified
          );

          // CALLBACK LIST OF CHOICES
          callback(valueOrValuesToSuggest);

        },
        // WITHOUT CREATING A CUSTOM FUNCTION JUST FOR THIS, IT COULD SIMPLY LOOK LIKE THIS (BUT ONLY EXTRACTS THE FIRST LEVEL OF KEYS)
        // choices: 'zeus+inputify.choices.valuesInCurrentlyEditedEntry(["edit.structure[0..-1].key"])',
      },
      {
        inputPreset: "zeusMethod(viewify.fieldDisplay)",
        label: "custom display",
        key: "display",
        help: "Choose here a custom display method for this field.",
      },
      {
        inputPreset: "zeusMethod(viewify.fieldPostprocess)",
        isAdvanced: true,
        key: "postprocess",
        help: "Choose here a method to postprocess this field dom element.",
      },
      {
        key: "showEvenIfUndefined",
        isAdvanced: true,
        help: 'If true, will not hide this field if the value is undefined.',
        inputifyType: "boolean",
      },

      //
      //                              LABEL

      { key: "LABEL", inputifyType: "intertitle", },

      {
        key: "label",
        inputifyType: "normal",
        help: [
          'A label/title to display before the content. Will display "label: value" instead of only the value (e.g. "date: 1873-10-02" instead of just "1873-10-02").',
          "If labelLayout is not hidden, and label is not defined, will use deepKey as label."
        ],
      },
      {
        key: "labelLayout",
        help: "How to display the label, in the same line, or in it's own line (by default label is hidden).",
        inputifyType: "radio",
        prepareValue: "hidden",
        choices: ["hidden", "stacked", "inline"],
      },

      //
      //                              STYLES

      { key: "STYLES AND LAYOUT", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "class",
        isAdvanced: true,
        help: "Class to apply to this field.",
        inputifyType: "normal",
      },
      {
        key: "css",
        isAdvanced: true,
        help: "Css styles to apply to this field.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "keyCss",
        isAdvanced: true,
        help: "Css styles to apply to this field key.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },

      //                              ¬
      //

    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
