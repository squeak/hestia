var _ = require("underscore");
var $$ = require("squeak");
var inputify = require("inputify");
inputify.inputsList.entry.specificOpts.push("entryType"); // adding this specific option that exist only for zeus, allows zeus to properly show only the appropriate inputs depending on the chosen inputifyType

var presetSandals = require("../../sandals");
var allInputifyGroups = _.chain(inputify.inputsList).pluck("specificOpts").flatten().uniq().value();
var inputsList = _.map(inputify.inputsList, function (obj, name) {
  return {
    _isChoice: true,
    label: '<span>'+ name +'<br><small>'+ obj.description +'</small></span>',
    value: name,
  };
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SHAPE IS OBJECT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function shapeIsNotObject () {
  var inputified = this;

  // shape defined
  var shapeValue = inputified.getSiblingValue("shape");
  if (shapeValue) {
    if (shapeValue == "array") return true
    else if (shapeValue == "object") return false;
  };

  // check from parent if this input is not an object
  var entryType = inputified.storage("parentInputified").getSibling("inputifyType").get();
  return entryType !== "object";

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SHAPE IS OBJECT, AND HAS NO STRUCTURE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function isObjectWithoutStructure () {
  var inputified = this;

  if (shapeIsNotObject.call(inputified, true)) return false
  else {
    var structureValue = inputified.getSiblingValue("structure");
    if (!structureValue || !structureValue.length) return true
    else return false;
  };

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_inputify_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: "&lt;INPUT&gt;<br>here you can setup the type of input to use (a text input, slider...)",
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      // o            >            +            #            °            ·            ^            :            |
      //                                           GENERAL

      { key: "GENERAL", inputifyType: "intertitle", },
      {
        key: "key",
        help: [
          "The key at which this value will be stored in entries.",
          '<span class="error-text">⚠️ If you modify this after having created entries, you will need to convert your entries manually in order not to lose values you have entered before modification.</span>',
        ],
        inputifyType: "normal",
        mandatory: true,
        valueOutVerify: function (val) {
          var inputified = this;
          if (!val) throw "you must set an input key"
          else if (val.match(/^_/)) throw "input key cannot start with underscore"
          else if (_.indexOf(["path", "type", "system"], val) != -1) throw "'path', 'type' and 'system' input keys are reserved, you cannot use them"
          else return val;
        },
      },
      {
        key: "inputifyType",
        inputifyType: "select",
        defaultValue: "normal",
        help: [
          "The type of input to use.",
          '<span class="error-text">⚠️ If you modify this after having created entries, make sure the new input type is compatible with the previous one you used, or convert your entries so they work with this new input type.</span>',
        ],
        choices: inputsList,
        // show hide some options depending on the group
        modified: function (value) {
          var inputified = this;

          // hide all
          var specificOptsIntertitle = inputified.getSibling("inputTypeSpecificOptionsIntertitle");
          if (specificOptsIntertitle) specificOptsIntertitle.hide();
          _.each(allInputifyGroups, function (groupName) {
            var sibglingInputified = inputified.getSibling(groupName);
            if (sibglingInputified) sibglingInputified.hide();
          });

          // show only the ones valid for this input type
          if (value) {
            if (specificOptsIntertitle && $$.getValue(inputify.inputsList[value], "specificOpts.length")) specificOptsIntertitle.show();
            _.each(inputify.inputsList[value].specificOpts, function (groupName) {
              var sibglingInputified = inputified.getSibling(groupName);
              if (sibglingInputified) sibglingInputified.show();
            });
          };

        },
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           PRESETS

      { key: "USE SOME PRESETS", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "inputPreset",
        isAdvanced: true,
        help: [
          "Instead of defining all the following options, you can just choose one of the following preset inputs types.",
          "(you may also pass options below, and they will be added to this preset's options)",
        ],
        inputifyType: "select",
        choices: _.keys(presetSandals),
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           DETAILS

      { key: "DETAILS", inputifyType: "intertitle", },
      {
        key: "help",
        help: "A help message to display when the input label is hovered.",
        inputifyType: "normal", // TODO: could as well be a function, if necessary change this to use zeusMethod(inputify[...]) instead
      },
      {
        key: "mandatory",
        help: "If this is true, the user will not be able to save the entry without setting a value to this input.",
        inputifyType: "boolean", // TODO: could as well be a function, if necessary change this to use zeusMethod(inputify[...]) instead, but would need to improve it so it's a boolean and not a random value
      },
      {
        key: "condition",
        isAdvanced: true,
        help: "Specify here some condition to decide if this input should be displayed or not.",
        inputPreset: 'zeusMethod(inputify.condition)',
      },
      {
        key: "isAdvanced",
        isAdvanced: true,
        help: "If this is true, the input will only be editable in advanced mode.",
        inputifyType: "boolean",
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           LABEL

      { key: "LABEL", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "label",
        help: [
          "The label of this input:",
          '<img src="/share/help/inputify-label.png">',
          "If you don't set any label, will just display the key.",
        ],
        inputifyType: "normal",
      },
      {
        key: "labelLayout",
        help: "How to display the label.",
        inputifyType: "radio",
        choices: ["inline", "stacked", "intertitle", "hidden"],
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           STYLINGS

      { key: "STYLINGS", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "cssInput",
        isAdvanced: true,
        help: "Custom css styles to apply to the input.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "cssLabel",
        isAdvanced: true,
        help: "Custom css styles to apply to the input label.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "cssContainer",
        isAdvanced: true,
        help: "Custom css styles to apply to the input container.",
        inputifyType: "object",
        object: {
          newKeyChoices: 'zeus+inputify.objectKeyChoices.cssProperties()',
          newKeyConstraint: 'zeus+inputify.objectKeyConstraint.keyIsInChoices()',
        },
      },
      {
        key: "attributes",
        isAdvanced: true,
        help: "Custom attributes to pass to the input html element.",
        inputifyType: "object",
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           VALUE

      { key: "VALUE", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "value",
        help: "The value set to this input on start.",
        inputifyType: "any",
        isAdvanced: true,
      },
      {
        key: "defaultValue",
        isAdvanced: true,
        help: "If this key has no value set, this value will be set instead.",
        inputPreset: 'zeusMethod(inputify.value, "any")',
      },
      {
        key: "defaultOnlyForNew",
        isAdvanced: true,
        inputifyType: "boolean",
        help: [
          "If this is set to true, the default value will only be set one time, when the entry is created.",
          "If this is set to false, every time the entry is edited, if this key is missing, the default value will be set.",
        ],
      },
      {
        key: "prepareValue",
        isAdvanced: true,
        help: "If value and defaultValue are undefined, will use this value to initialize input, but if this value is not modified, it will not be saved.",
        inputPreset: 'zeusMethod(inputify.value, "any")',
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           VALUE VERIFICATION AND PROCESSING

      // { key: "VALUE VERIFICATION AND PROCESSING", inputifyType: "intertitle", },
      //
      // {
      //   key: "valueInVerify",
      //   help: "if defined, this function must return something, use this to discard a value that wouldn't be appropriate and prevent setting it, or to filter it and make it appropriate, for real value modification, use valueInProcess",
      //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
      // },
      // {
      //   key: "valueOutVerify",
      //   help: "- if defined, this function must return something, use this to discard a value that wouldn't be appropriate and prevent getting it (get will return undefined), or to filter it and make it appropriate (for real value modification, use valueOutProcess)\n- to display that the value is invalid, throw an error here",
      //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
      // },
      // {
      //   key: "valueInProcess",
      //   help: "the given value will be processed with this function",
      //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
      // },
      // {
      //   key: "valueOutProcess",
      //   help: "the chosen value will be processed with this function",
      //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
      // },
      // {
      //   key: "unvalidatedThrowError",
      //   help: "if set to true and there is an error validating value while processing in or out, an error will be thrown",
      //   inputifyType: "boolean",
      //   prepareValue: false,
      // },
      // {
      //   key: "unvalidatedAlertError",
      //   help: "if set to false will not alert when error validating value in or out",
      //   inputifyType: "boolean",
      //   prepareValue: true,
      // },
      // {
      //   key: "unvalidatedOnChangedSilent",
      //   help: "if true, will not alert or throw error if not validated on changed event",
      //   inputifyType: "boolean",
      //   prepareValue: true,
      // },

      // o            >            +            #            °            ·            ^            :            |
      //                                           EVENTS

      { key: "EVENTS", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "changed",
        help: "executed when value is modified by the user",
        inputPreset: 'zeusMethod(inputify.modified)',
        isAdvanced: true,
      },
      {
        key: "setCallback",
        help: "executed when value has been set programatically",
        inputPreset: 'zeusMethod(inputify.modified)',
        isAdvanced: true,
      },
      {
        key: "modified",
        help: "executed when value is modified by the user or has been set programatically (modified is executed before changed or setCallback)",
        inputPreset: 'zeusMethod(inputify.modified)',
        isAdvanced: true,
      },
      {
        key: "created",
        help: "executed when the input has been created (only on first creation, not on remakes)",
        inputPreset: 'zeusMethod(inputify.modified)',
        isAdvanced: true,
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           SIBLING SUBSCRIPTIONS

      { key: "SIBLINGS SUBSCRIPTIONS", inputifyType: "intertitle", isAdvanced: true, },
      {
        key: "siblingsSubscriptions",
        help: "subscribe to some events in sibling inputs",
        inputifyType: "array",
        labelLayout: "hidden",
        isAdvanced: true,
        object: {
          model: {
            inputifyType: "entry", /// TODO: could also be a function returning an array of subscriptions
            object: {
              structure: [
                {
                  key: "key",
                  help: "The key of the other input you want to subscribe to.",
                  inputifyType: "normal",
                },
                {
                  key: "event",
                  help: "The event you want to subsribe to.",
                  inputifyType: "radio",
                  choices: ["modified", "changed", "setCallback"],
                },
                {
                  key: "action",
                  help: "The callback to execute on this input when the event has been fired in the sibling.",
                  inputifyType: "select",
                  choices: ["remake", "refreshChoices", "recalculateDisplayCondition", "recalculateLockState", "show", "hide", "toggle"],
                  // TODO: could also be directly a function: function(inputThatFiredTheEvent<inputified>){@this=<inputified>« the input that subscribed to the event firing »}
                },
              ],
            },
          },
        },
      },

      //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
      //                                                  OPTIONS USED ONLY IN SOME SPECIFIC INPUT TYPES
      //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

      {
        label: "THIS INPUT TYPE SPECIFIC OPTIONS",
        key: "inputTypeSpecificOptionsIntertitle",
        inputifyType: "intertitle",
        condition: false, // hidden on start
      },

      // CHOICES
      {
        key: "choices",
        condition: false, // hidden on start
        help: "The list of item that are suggested or chosen from.",
        inputPreset: 'zeusMethod(inputify.choices, "array")',
      },

      // INPUT
      {
        key: "input",
        condition: false, // hidden on start
        help: "Customize the html input tag.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "type",
              help: "Html input type: e.g. password, number, color...",
              inputifyType: "normal",
            },
            {
              key: "placeholder",
              help: "Placeholder value before the user types in anything.",
              inputifyType: "normal",
            },
          ],
        },
      },

      // MONOSPACE
      {
        key: "monospace",
        condition: false, // hidden on start
        help: "Use a monospace font for this input.",
        inputifyType: "boolean",
      },

      // SPECTRUM
      {
        key: "spectrum",
        condition: false, // hidden on start
        help: "Color input options.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "showChoicesPalette",
              help: "If true, will display a palette of colors to choose and more button to choose any custom color.",
              inputifyType: "boolean",
            },
          ],
        },
      },

      // DATE
      {
        key: "date",
        condition: false, // hidden on start
        help: "Date input options.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "precision",
              help: "Precision of the date to choose.",
              inputifyType: "radio",
              choices: ["year", "month", "date", "time"],
              defaultValue: "date",
            },
            {
              key: "precisionChoiceDisable",
              help: "Disable the possibility for the user to choose the precision.",
              inputifyType: "boolean",
            },
            {
              key: "rangePicker",
              help: "Initial state of the date picker, date or range.",
              inputifyType: "boolean",
            },
            {
              key: "rangeChoiceDisable",
              help: "Disable the possibility for the user to choose to input range or date.",
              inputifyType: "boolean",
            },
          ],
        },
      },

      // TIME
      {
        key: "time",
        condition: false, // hidden on start
        help: "Time input options.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "parts",
              help: "The elements you want this time input to allow to edit (hours, minutes, seconds), in the order they should appear in the time string.\n\nExample:\nIf you want your time string to look like \"seconds:minutes:hours\", set [\"seconds\", \"minutes\", \"hours\"].",
              inputifyType: "suggest",
              choices: ["hours", "minutes", "seconds"],
              defaultValue: ["hours", "minutes"],
            },
          ],
        },
      },

      // SLIDER
      {
        key: "slider",
        condition: false, // hidden on start
        help: "Slider input options.",
        inputifyType: "entry",
        object: {
          structure: [
            "orientation",
            {
              key: "tooltips",
              help: "Show tooltip(s) on each handle\n(boolean or array of booleans).",
              isAdvanced: true,
              inputifyType: "any",
            },
            {
              key: "min",
              help: "Minimum slider value.",
              inputifyType: "number",
            },
            {
              key: "max",
              help: "Maximum slider value.",
              inputifyType: "number",
            },
            {
              key: "step",
              help: "Steps between values.",
              inputifyType: "number",
            },
            {
              key: "range",
              help: [
                'Use this option (instead of "min", "max" and "step") to create more comple sliders, with variable steps.',
                'Keys of this object should be percentages (e.g. "20%) and "min" and "max".',
                'Values should be like the following: [',
                '&emsp;&lt;number&gt; « beginning value of this part of the range »,',
                '&emsp;&lt;number&gt; « step size to use in this part of the range (omit it in "max") »,',
                ']',
                ' ',
                "For example, the following range option:",
                'range: {',
                '&emsp;"min": [0, 1],',
                '&emsp;"50%": [50, 5],',
                '&emsp;"75%": [100, 10],',
                '&emsp;"max": [1000]',
                '}',
                "will create a slider from 0 to 1000.",
                "The first half of the slider will go from 0 to 50 with steps of 1.",
                "Then the third quarter will go from 50 to 100 with steps of 5.",
                "And the last quarter will go from 100 to 1000 with steps of 10.",
              ],
              inputifyType: "object",
              object: {
                model: {
                  inputifyType: "array",
                  object: {
                    structure: [
                      { key: "0", label: "from", inputifyType: "number", },
                      { key: "1", label: "step", inputifyType: "number", },
                    ]
                  },
                },
              },
              isAdvanced: true,
            },
          ],
        },
      },

      // SORTABLELIST
      {
        key: "sortableList",
        condition: false, // hidden on start
        help: "sortableList input options",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "handle",
              help: [
                "If not defined the whole entry can be grabbed.",
                'If you want to use the default handle, pass ".handle".',
                "If you want to use a custom handle, pass any other selector.",
              ],
              inputifyType: "normal",
            },
            {
              key: "display",
              help: [
                "If this is specified, it will be used to map the list for custom displaying",
                "Pass a key here to pluck list with.",
                "If display is undefined, the list will be displayed raw.",
              ],
              inputifyType: "normal",
              isAdvanced: true,
            },
            {
              key: "liClass",
              help: "Use this to specify a class to apply to the li elements.",
              inputifyType: "normal",
              isAdvanced: true,
            },
          ],
        },
      },

      // SUGGEST
      {
        key: "suggest",
        condition: false, // hidden on start
        help: "Suggest-like input options.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "maxItemCount",
              help: "How many items the user can input, if -1 no limit.",
              inputifyType: "number",
            },
            // {
            //   key: "valueToLabel",
            //   help: "How to display a value label.",
            //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
            // },
            // {
            //   key: "",
            //   help: "This function allow you to define custom ways to interpret the inputed value to convert it into a choice label\nthrow an error in this function to prevent the choice from being created (in other words to discard the currently inputed value).",
            //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
            // },
          ],
        },
      },

      // RADIOTICKBOX
      {
        key: "radioTickbox",
        condition: false, // hidden on start
        help: "Radio and tickbox input options.",
        inputifyType: "entry",
        object: {
          structure: [
            "orientation",
            // {
            //   key: "valueToLabel",
            //   help: "how to display a value label",
            //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
            // },
            // {
            //   key: "customItemDisplay",
            //   help: "if this is defined, on top of the previous valueToLabel option, you can create your own customized display for the item's text/content (icons and sortOrder display are still automatically displayed)",
            //   // inputPreset: 'zeusMethod(inputify[...])', // TODO
            // },
            {
              key: "noText",
              help: "If true, will not display choices text.",
              inputifyType: "boolean",
            },
            {
              key: "iconChecked",
              help: "Use this if you want to customize the icon for checked state.",
              inputifyType: "icon",
            },
            {
              key: "iconUnchecked",
              help: "Use this if you want to customize the icon for unchecked state (by default there is none).",
              inputifyType: "icon",
            },
            {
              key: "sortBySelectOrder",
              help: "If true, will order tickbox selected value in the order they have been selected.",
              inputifyType: "boolean",
              prepareValue: false,
            },
          ],
        },
      },

      // BOOLEAN
      {
        key: "boolean",
        condition: false, // hidden on start
        help: "Boolean input options.",
        inputifyType: "entry",
        object: {
          structure: [
            "orientation",
            {
              key: "iconOn",
              help: "Use this if you want to customize the icon for boolean on state.",
              inputifyType: "icon",
              prepareValue: "toggle-on",
            },
            {
              key: "iconOff",
              help: "Use this if you want to customize the icon for boolean off state.",
              inputifyType: "icon",
              prepareValue: "toggle-off",
            },
          ],
        },
      },

      // NUMBER
      {
        key: "number",
        condition: false, // hidden on start
        help: "Number input options.",
        inputifyType: "entry",
        object: {
          structure: [
            "orientation",
            {
              key: "min",
              help: "Minimum number that can be inputed.\nSet null if you want no minimum limit.",
              inputifyType: "any",
              prepareValue: 0,
            },
            {
              key: "max",
              help: "Maximum number that can be inputed.\nSet null if you want no maximum limit.",
              inputifyType: "any",
              prepareValue: null,
            },
            {
              key: "step",
              help: "How much will be added/subtracted to the value when +- buttons are clicked.",
              inputifyType: "number",
            },
          ],
        },
      },

      // GEO
      {
        key: "geo",
        condition: false, // hidden on start
        help: "Geo input options.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "shape",
              help: "The types of shape that this input allow to create.",
              inputifyType: "tickbox",
              choices: ["point", "circle", "polyline", "polygon"],
            },
          ],
        },
      },

      // FILE
      {
        key: "file",
        condition: false, // hidden on start
        help: "file input options",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "title",
              help: "Switch this to true if you want the user to have the possibility to give a title to the file.",
              inputifyType: "boolean",
            },
            {
              key: "fileTypes",
              help: [
                'List of allowed file types or extensions (e.g. ["jpg", "png"])',
                "Extensions will be lowercased before comparison.",
                'Available file types are "image", "video" and "audio" (which match many types of image, video and audio files).',
                "If this option is not defined any file can be selected.",
              ],
              inputifyType: "multi",
              choices: ["image", "video", "audio"],
            },
            {
              key: "imagePreview",
              help: "If this is true or undefined, will display a little preview of the selected image.",
              inputifyType: "boolean",
            },
            {
              key: "process",
              inputifyType: "array",
              help: [
                "A list of functions to process the selected file before uploading.",
                "(The ordering of those functions is important: first in list will be the first applied to the image.)",
              ],
              isAdvanced: true,
              object: {
                model: {
                  inputPreset: 'zeusMethod(inputify.fileProcess)',
                  presetWithoutKey: true,
                },
              },
            },
            {
              key: "additionalInputs",
              inputifyType: "array",
              object: {
                model: {
                  inputifyType: "entry",
                  entryType: "_inputify_withOptionalKey_",
                },
              },
            },
          ],
        },
      },

      // ENTRY TYPE
      {
        key: "entryType",
        inputPreset: "availableTypes",
        inputifyType: "select",
        condition: false, // hidden on start
        help: "Choose an entry type that you created in your types database.",
        isAdvanced: true,
      },

      // THE FOLLOWING WAS DISABLED IN dato-1.6.0 BECAUSE IT SEEMS QUITE COMPLICATED TO EVER UNDERSTAND AND USE FOR ANY USER
      // // ENTRY
      // {
      //   key: "entry",
      //   inputifyType: "entry",
      //   help: "Options for entry-like inputs.",
      //   isAdvanced: true,
      //   condition: false, // hidden on start
      //   object: {
      //     structure: [
      //
      //       // CUSTOMIZED OPTIONS
      //       {
      //         key: "customizedOptions",
      //         help: [
      //           "If this is set, the entry input opened in the dialog will use these custom settings.",
      //           "On the contrary, when this is not set, the dialog input is an object and gets options.object passed by its maker.",
      //         ],
      //         inputifyType: "entry",
      //         entryType: "_inputify_withOptionalKey_",
      //       },
      //
      //
      //     ],
      //   },
      // },

      // OBJECT
      {
        key: "object",
        condition: false, // hidden on start
        help: "Options for object-like inputs.",
        inputifyType: "entry",
        object: {
          structure: [

            //
            //                              SHAPE

            {
              label: "SHAPE",
              labelLayout: "intertitle",
              key: "shape",
              help: "Is this object an [array] or an {object}.",
              inputifyType: "radio",
              choices: ["object", "array"],
              condition: function () {
                var inputified = this;
                var entryType = inputified.storage("parentInputified").getSibling("inputifyType").get();
                return entryType == "entry"
              },
            },

            //
            //                              STRUCTURE

            {
              label: "STRUCTURE",
              labelLayout: "intertitle",
              key: "structure",
              help: [
                "Defines the list of inputs that lets you edit the object/array.",
                "(If shape is an array, you don't need to specify keys in subinputs.)",
              ],
              inputifyType: "array",
              object: {
                model: {
                  inputifyType: "entry",
                  entryType: "_inputify_withOptionalKey_",
                },
              },
            },

            //
            //                              MODEL

            {
              label: "MODEL",
              labelLayout: "intertitle",
              key: "model",
              help: [
                "If you want all inputs in this object/array to be of the same type, you can specify it here.",
                "If structure is defined, this input configuration will be used for new custom keys.",
              ],
              inputifyType: "entry",
              entryType: "_inputify_withoutKey_",
            },

            //
            //                              SORT DISPLAY

            {
              label: "SORT DIALOG DISPLAY",
              labelLayout: "intertitle",
              key: "sortDisplay",
              siblingsSubscriptions: [ { key: "shape", event: "modified", action: "remake", }, ],
              condition: shapeIsNotObject,
              help: [
                "You can use this to customize what's displayed when the user asks to reorgannize entries in this array.",
                "When the reogrannize dialog is opened, it lists the entries in this array so you can grab them and change their order. That list just show one line of each entry.",
                "Set here the key where to find in your entry the content that should be shown in that line.",
              ],
              inputifyType: "normal",
            },

            //
            //                              NEW KEY SUGGESTIONS AND CONSTRAINT

            {
              label: "KEYS CHOICES",
              labelLayout: "intertitle",
              key: "newKeyChoices",
              isAdvanced: true,
              help: "Use this to propose some suggested keys for this object.",
              inputPreset: 'zeusMethod(inputify.objectKeyChoices)',
              condition: isObjectWithoutStructure,
              siblingsSubscriptions: [
                { key: "shape", event: "modified", action: "remake", },
                { key: "structure", event: "modified", action: "remake", },
              ],
            },

            {
              label: "KEYS CONSTRAINT",
              labelLayout: "intertitle",
              key: "newKeyConstraint",
              isAdvanced: true,
              help: "Use this option to constrain the values that can be used as keys for this object.",
              inputPreset: 'zeusMethod(inputify.objectKeyConstraint)',
              condition: isObjectWithoutStructure,
              siblingsSubscriptions: [
                { key: "shape", event: "modified", action: "remake", },
                { key: "structure", event: "modified", action: "remake", },
              ],
            },

            //                              ¬
            //

          ],
        },
      },

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      // o            >            +            #            °            ·            ^            :            |
      //                                           STYLING AND COLORING

      { key: "STYLING AND COLORING", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "containerClass",
        isAdvanced: true,
        help: "Additional html class to apply to the input container.",
        inputifyType: "normal",
      },
      {
        key: "inputClass",
        isAdvanced: true,
        help: "Additional html class to apply to the input.",
        inputifyType: "normal",
      },
      {
        key: "color",
        isAdvanced: true,
        help: "Apply custom colors to this input.",
        inputifyType: "entry",
        object: {
          structure: [
            {
              key: "base",
              help: "Base color of the backgrounds and neutral elements in the input.",
              inputifyType: "color",
            },
            {
              key: "baseContrast",
              help: "Contrast color for base.",
              inputifyType: "color",
            },
            {
              key: "primary",
              help: "This will color the input.",
              inputifyType: "color",
            },
            {
              key: "primaryContrast",
              help: "This will be the color contrast for primary, for example if primary colors an element background, primaryContrast would be applied to this element text.",
              inputifyType: "color",
            },
            {
              key: "hover",
              help: "A color to highlight some parts of the input when the user hovers over them.\nIf primary is set, but not hover, will remove any hovering effect.",
              inputifyType: "color",
            },
            {
              key: "hoverContrast",
              help: "Just like primaryContrast but for hovered elements.",
              inputifyType: "color",
            },
          ],
        },
      },

      // o            >            +            #            °            ·            ^            :            |

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
