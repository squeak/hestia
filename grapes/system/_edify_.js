
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  name: "_edify_",
  group: "_system",

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: {
    help: "&lt;LIST OF INPUTS&gt;<br>here you can setup the way your entry will be edited, the list of inputs the user will have to fill",
    structure: [
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

      //
      //                              STRUCTURE

      { key: "STRUCTURE", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "structure",
        inputifyType: "array",
        labelLayout: "hidden",
        help: [
          "The list of keys the user will have to input for this type of entry",
          "(If this is undefined, the list of keys is free, and they have to be added manually.)",
        ],
        object: {
          model: {
            inputifyType: "entry",
            entryType: "_inputify_",
          },
          sortDisplay: "key",
        },
      },

      //
      //                              MODEL

      { key: "MODEL", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "model",
        inputifyType: "entry",
        labelLayout: "hidden",
        help: [
          "If you don't want the entry to have a fixed structure, you can pass a model of input configuration here.",
          "Every key the user then adds to the entry will be edited with this input configuration.",
        ],
        entryType: "_inputify_withoutKey_",
        isAdvanced: true,
      },

      //
      //                              MORE

      { key: "ADVANCED", inputifyType: "intertitle", isAdvanced: true, },

      {
        key: "help",
        isAdvanced: true,
        help: "Pass here a note/comment/message that will be shown in top of the edition page (e.g. \"don't edit this entry unless you're really sure of what you're doing\")",
        inputifyType: "normal",
      },
      {
        key: "beforeSave",
        isAdvanced: true,
        help: "A function executed before saving an entry (saving will only take place if the passed callback is executed).",
        inputPreset: 'zeusMethod(edify.beforeSave)',
      },
      {
        key: "beforeRemove",
        isAdvanced: true,
        help: "A function executed before removing an entry (removal will only take place if the passed callback is executed).",
        inputPreset: 'zeusMethod(edify.beforeRemove)',
      },
      {
        inputPreset: "buttons_edify",
        labelLayout: "intertitle",
        isAdvanced: true,
        help: "A list of custom buttons to display in the menu bar.",
      },

      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    ],
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
