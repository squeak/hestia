var _ = require("underscore");
var $$ = require("squeak");

var inputifyWithOptionalKey = $$.clone(require("./_inputify_"), 4);
inputifyWithOptionalKey.name = "_inputify_withOptionalKey_";
delete inputifyWithOptionalKey.edit.help;
delete inputifyWithOptionalKey.edit.structure[1].mandatory;
delete inputifyWithOptionalKey.edit.structure[1].valueOutVerify;
delete inputifyWithOptionalKey.edit.structure[1].help;

module.exports = inputifyWithOptionalKey
