
/**
  DESCRIPTION: create a button type customized as asked
  ARGUMENTS: (
    !name <string> « the name of this type »,
    !clickAction_deepKeyInDescartes <string> « key where to find available actions on button click (in descartes, e.g. "tablify.cellDisplay") »,
  )
  RETURN: <grape>
*/
module.exports = function (name, clickAction_deepKeyInDescartes, condition_deepKeyInDescartes) {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    name: name,
    group: "_system",

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  EDIT
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    edit: {
      structure: [
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

        { key: "GENERAL", inputifyType: "intertitle", },

        {
          key: "icomoon",
          label: "Icon: ",
          inputifyType: "icon",
          help: "Button's icon.",
        },

        {
          key: "title",
          label: "Title: ",
          inputifyType: "normal",
          help: "Text displayed when button is hovered."
        },

        {
          key: "key",
          label: "Shortcut combo: ",
          inputifyType: "normal",
          help: [
            "Shortcut key combo to call this action.",
            "It's recommended not to use key combo that could interfere with a system or browser.",
            "In other words, it's better to avoid using modifier keys, appart from shift.",
          ],
        },

        {
          key: "condition",
          label: "Condition: ",
          help: "Conditions that should be validated to display this button.",
          inputPreset: 'zeusMethod('+ condition_deepKeyInDescartes +')',
        },

        {
          key: "click",
          help: "action executed when button is clicked",
          labelLayout: "intertitle",
          inputPreset: 'zeusMethod('+ clickAction_deepKeyInDescartes +')',
        },

        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
      ],
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};
